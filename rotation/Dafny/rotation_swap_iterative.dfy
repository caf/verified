// Copyright 2014 Carlo A. Furia


// \rho^r rotation function
function rot(S: seq<int>, r: int): seq<int>
   requires 0 <= r < |S|;
   ensures |S| == |rot(S, r)|;
{
   S[r..|S|] + S[0..r]
}

ghost method left_is_smaller(X: seq<int>, Y: seq<int>, Z: seq<int>, d: int)
   requires |X| == |Z| == d;
   requires |Y| > 0;
   ensures rot(X + Y + Z, d) == rot(Z + Y, d) + X;
{
}

ghost method right_is_smaller(X: seq<int>, Y: seq<int>, Z: seq<int>, d: int)
   requires 0 < |X| == |Z| == d;
   requires |Y| > 0;
   ensures |X| + |Y| + |Z| - 2*d < |X| + |Y| + |Z| - d;
   ensures rot(X + Y + Z, |X| + |Y| + |Z| - d) == Z + rot(Y + X, |X| + |Y| + |Z| - 2*d);
{
}


// swap a[low..low + d) and a[high - d..high)
method swap_sections(a: array<int>, low: int, high: int, d: int)
   requires a != null;
   requires 0 <= low <= low + d <= high - d <= high <= a.Length;
   modifies a;
   ensures a[low..low + d] == old(a[high - d.. high]);
   ensures a[low + d..high - d] == old(a[low + d..high - d]);
      // redundant with the previous, but useful to switch from slice to quantified representation
   ensures forall i :: low + d <= i < high - d ==> a[i] == old(a[i]);
   ensures a[high - d..high] == old(a[low..low + d]);
      // redundant, but useful all around
   ensures a[low..high] == old(a[high - d.. high]) + old(a[low + d..high - d]) + old(a[low..low + d]);
   ensures forall i :: 0 <= i < low ==> a[i] == old(a[i]);
   ensures forall i :: high <= i < a.Length ==> a[i] == old(a[i]);
{
   var x: int, z: int;

   x, z := low, high - d;
   while x < low + d
   invariant low <= x <= low + d;
   invariant high - d <= z <= high;
   invariant x - low == z - (high - d);
      // explicit quantification works much better here
   invariant forall i :: low <= i < x ==> a[i] == old(a[..])[high - d + i - low];
   invariant forall i :: x <= i < high - d ==> a[i] == old(a[..])[i];
   invariant forall i :: high - d <= i < z ==> a[i] == old(a[..])[low + i - (high - d)];
   invariant forall i :: z <= i < high ==> a[i] == old(a[..])[i];
   // frame invariants
   invariant forall i :: 0 <= i < low ==> a[i] == old(a[i]);
   invariant forall i :: high <= i < a.Length ==> a[i] == old(a[i]);
   {
      // swap a[x] and a[z]
      a[x], a[z] := a[z], a[x];
      x, z := x + 1, z + 1;
   }
}


// Left-rotate a by r by swapping equal segments iteratively
method rotate_swap_iterative(a: array<int>, r: int)
   requires a != null;
   requires 0 <= r && r < a.Length;
   modifies a;
   ensures a.Length == old(a.Length);
   ensures a[..] == rot(old(a[..]), r);
{
   if r == 0 {
      return;
   }

   var low: int, high: int, p: int;

   low, p, high := 0, r, a.Length;
   while low < p < high
   invariant 0 <= low <= p <= high <= a.Length;
   invariant low == p <==> p == high;
      // loop independent, but badly needed for performance
   invariant old(a[..]) == old(a[0..a.Length]);
   invariant forall i :: 0 <= i < low ==> a[i] == rot(old(a[..]), r)[i];
   invariant forall i :: high <= i < a.Length ==> a[i] == rot(old(a[..]), r)[i];
      // the following clause makes the invariant inductive
   invariant p - low < high - low ==> rot(old(a[..]), r)[low..high] == rot(a[low..high], p - low);
   decreases high - low;
   {
      if p - low == high - p {
         // swap a[low..p) and a[p..high)
         swap_sections(a, low, high, p - low);
         assert a[low..high] == rot(old(a[..]), r)[low..high];
         assert rot(old(a[..]), r) == 
                rot(old(a[..]), r)[0..low] 
                   + rot(old(a[..]), r)[low..high] 
                   + rot(old(a[..]), r)[high..a.Length];
         low, high := low + (p - low), high - (high - p);
      } else {
      if p - low < high - p {
         ghost var b := a[..];
         // swap a[low..p) and a[high - (p - low)..high]
         swap_sections(a, low, high, p - low);
         assert b[low..high] == b[low..p] + b[p..high - (p - low)] + b[high - (p - low)..high];
         left_is_smaller(b[low..p], b[p..high - (p - low)], b[high - (p - low)..high], p - low);
         assert a[low..p] + a[p..high - (p - low)] == a[low..high - (p - low)];
         assert rot(old(a[..]), r) == 
                rot(old(a[..]), r)[0..low] 
                   + rot(old(a[..]), r)[low..high] 
                   + rot(old(a[..]), r)[high..a.Length];
         high := high - (p - low);
      } else {
      if p - low > high - p {
         ghost var b := a[..];
         // swap a[low..low + (high - p)) and a[p..high)
         swap_sections(a, low, high, high - p);
         assert b[low..high] == b[low..low + (high -p)] + b[low + (high -p)..p] + b[p..high];
         right_is_smaller(b[low..low + (high - p)], b[low + (high - p)..p], b[p..high], high - p);
         assert rot(b[low..high], p - low) == 
                b[p..high] + rot(b[low + (high -p)..p] + b[low..low + (high -p)], 2*p - (low + high));
         assert a[low + (high - p)..p] + a[p..high] == a[low + (high - p)..high];
         assert rot(old(a[..]), r) == 
                rot(old(a[..]), r)[0..low] 
                   + rot(old(a[..]), r)[low..high] 
                   + rot(old(a[..]), r)[high..a.Length];
         low := low + (high - p);
      }}}
   }
}
