// Copyright 2014 Carlo A. Furia

// wrap function (incomplete definition)
function wrap(x: int, y: int): int
   requires 0 <= x && 0 < y;
{
   if x < y then x
            else x - y
}


// \rho^r rotation function
function rot(S: seq<int>, r: int): seq<int>
   requires 0 <= r < |S|;
   ensures |S| == |rot(S, r)|;
   ensures forall k :: 0 <= k < |S| ==> rot(S, r)[k] == S[wrap(k + r, |S|)];
   ensures forall k :: 0 <= k < |S| ==> S[k] == rot(S, r)[wrap(k + |S| - r, |S|)];
{
   S[r..|S|] + S[0..r]
}


// Copy b's content into a.
method copy(a: array<int>, b: array<int>)
   requires a != null && b != null;
   requires a.Length == b.Length;
   modifies a;
   ensures a != null;
   ensures a[..] == b[..];
{
   forall (i | 0 <= i < b.Length)
   {
      a[i] := b[i];
   }
}


// Left-rotate a by r by copying
method rotate_copy(a: array<int>, r: int)
   requires a != null;
   requires 0 <= r < a.Length;
   modifies a;
   ensures a.Length == old(a.Length);
   ensures a[..] == rot(old(a[..]), r);
{
   var N := a.Length;
   var b: array<int>;
   var s: int, d: int;

   if r == 0 {  // In this case, rotation coincide with identity
      return;
   }

   b := new int[N];
   s := 0; d := N - r;
   while s < N
   invariant 0 <= s <= N;
   invariant d == wrap(s + N - r, N);
   invariant forall i :: 0 <= i < s ==> a[i] == b[wrap(i + N - r, N)];
   invariant a[..] == old(a[..]);
   {
      b[d] := a[s];
      s, d := s + 1, d + 1;
      // wrap over a's bounds
      if d == N { d := 0; }
   }
   // copy b's content back into a
   copy(a, b);
   // inlining copy's body greatly slows down verification
}
