// Copyright 2014 Carlo A. Furia


// In this file, we could use 'cwrap' instead of 'wrap' (provided we rename it to 'wrap'!), 
// but with some performance penalty. See rotation_copy.dfy for the definition of 'cwrap'.

function wrap(x: int, y: int): int
   requires 0 <= x && 0 < y;
{
   if x < y then x
            else x - y
}

// \rho^r rotation function
function rot(S: seq<int>, r: int): seq<int>
   requires 0 <= r < |S|;
   ensures |S| == |rot(S, r)|;
   ensures forall k :: 0 <= k < |S| ==> rot(S, r)[k] == S[wrap(k + r, |S|)];
   ensures forall k :: 0 <= k < |S| ==> S[k] == rot(S, r)[wrap(k + |S| - r, |S|)];
{
   S[r..|S|] + S[0..r]
}

// Position k maps to in a reversal of [low..high)
function rp(k: int, low: int, high: int): int
   requires low <= k < high;
   ensures low <= rp(k, low, high) < high;
{
   high + low - 1 - k
}

// Inductive definition of reversal of a sequence
function rev(S: seq<int>): seq<int>
   ensures |rev(S)| == |S|;
{
   if |S| == 0 then S
               else rev(S[1..]) + [S[0]]
}

// rp and rev provide equivalent representations
ghost method rev_is_rp(S: seq<int>, T: seq<int>)
   requires |S| == |T|;
   requires forall i :: 0 <= i < |S| ==> S[i] == T[rp(i, 0, |S|)];
   ensures T == rev(S);
{
   if |S| > 0 {
      rev_is_rp(S[1..], T[0..|S| - 1]);
   }
}
   
ghost method lemma_rev_cat(S: seq<int>, T: seq<int>)
   ensures rev(S + T) == rev(T) + rev(S);
{
   if |S| > 1 {
//      assert rev(S + T) == rev((S + T)[1..]) + [S[0]];
      assert (S + T)[1..] == S[1..] + T;
//      lemma_rev_cat(S[1..], T);
   }
}

ghost method lemma_rev_rev(S: seq<int>)
   ensures rev(rev(S)) == S;
{
   if |S| > 0 {
      lemma_rev_cat(rev(S[1..]), [S[0]]);
   }
}


// reverse a[low..high) in place
method reverse(a: array<int>, low: int, high: int)
   requires a != null;
   requires 0 <= low <= high <= a.Length;
   modifies a;
   ensures a != null;
   ensures a.Length == old(a.Length);
   ensures a[low..high] == rev(old(a[..])[low..high]);
     // same meaning as previous postcondition, but with explicit quantification
   ensures forall i :: low <= i < high ==> old(a[..])[i] == a[rp(i, low, high)];
   ensures forall i :: 0 <= i < low ==> a[i] == old(a[..])[i];
   ensures forall i :: high <= i < a.Length ==> a[i] == old(a[..])[i];
{
   var p: int, q: int;

   p, q := low, high - 1;
   while p < q + 1
   invariant low <= p <= q + 2 <= high + 1;
   invariant q == high + low - 1 - p;
   invariant forall i :: low <= i < p ==> old(a[..])[i] == a[rp(i, low, high)];
   invariant forall i :: q < i < high ==> old(a[..])[i] == a[rp(i, low, high)];
   // frame invariants
   invariant forall i :: p <= i <= q ==> old(a[..])[i] == a[i];
   invariant forall i :: 0 <= i < low ==> old(a[..])[i] == a[i];
   invariant forall i :: high <= i < a.Length ==> old(a[..])[i] == a[i];
   {
      // swap a[p] and a[q]
      a[p], a[q] := a[q], a[p];
      p, q := p + 1, q - 1;
   }
   rev_is_rp(old(a[..])[low..high], a[low..high]);
}


// Left-rotate a by r by performing three reversals.
// Key correctness argument: lemma_rev_cat for |S| = r and |T| = a.Length - r
method rotate_reverse(a: array<int>, r: int)
   requires a != null;
   requires 0 <= r < a.Length;
   modifies a;
   ensures a.Length == old(a.Length);
   ensures a[..] == rot(old(a[..]), r);
{
   reverse(a, 0, r);
   assert old(a[..])[r..a.Length] == a[r..a.Length];
   reverse(a, r, a.Length);
   assert a[..] == rev(old(a[..])[0..r]) + rev(old(a[..])[r..a.Length]);
   lemma_rev_cat(a[0..r], a[r..a.Length]);
   assert a[..] == a[0..a.Length];
   reverse(a, 0, a.Length);
   assert a[..] == rev(rev(old(a[..])[r..a.Length])) + rev(rev(old(a[..])[0..r]));
   lemma_rev_rev(old(a[..])[r..a.Length]);
   lemma_rev_rev(old(a[..])[0..r]);
}
