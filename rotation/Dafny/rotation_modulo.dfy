// Copyright 2014 Carlo A. Furia


// Importing from module ModularRotation does not work because the
// proofs here require the explicit definition of 'mp' but imports are
// always opaque (use postconditions only). Embedding mp's definition in
// its postcondition also doesn't work, because it triggers many otiose
// instantiations.  Hence, I just copy-pasted signatures here.

// \rho^r rotation function
function rot(S: seq<int>, r: int): seq<int>
   requires 0 <= r < |S|;
   ensures |S| == |rot(S, r)|;

// rotation by zero is the identity
ghost method rotate_zero(S: seq<int>)
   requires 0 < |S|;
   ensures rot(S, 0) == S;

function gcd(x: int, y: int): int
   requires 0 < x && 0 < y;
   ensures 0 < gcd(x, y) <= x && gcd(x, y) <= y;

function tau(x: int, y: int): int
   requires 0 < x && 0 < y;
   ensures gcd(x, y) * tau(x, y) == x;
   ensures 0 < tau(x, y) <= x;

function mp(N: int, M: int, S: int, p: int): int
   requires 0 < M < N;
   requires 0 <= S < N;
   requires 0 <= p;
   ensures 0 <= mp(N, M, S, p) < N;
{  // Inlining wrap is more efficient
   if p == 0 then S else 
      (if mp(N, M, S, p - 1) + M < N then mp(N, M, S, p - 1) + M 
                                     else mp(N, M, S, p - 1) + M - N)
}

// Elements of cycles with different starts S are disjoint
ghost method lemma_mp_disjoint_cycles(N: int, M: int, S: int, K: int)
   requires 0 < M < N;
   requires 0 <= S < gcd(N, M);
   requires 0 <= K;
   ensures forall t, q :: 0 <= q < tau(N, M) && S < t < gcd(N, M) 
      ==> mp(N, M, S, K) != mp(N, M, t, q);

// Any cycle goes back to start S after tau(N, M)
ghost method lemma_mp_complete_cycle(N: int, M: int, S: int)
   requires 0 < M < N;
   requires 0 <= S < gcd(N, M);
   ensures mp(N, M, S, 0) == mp(N, M, S, tau(N, M));

// tau(N, M) elements in each cycle are all different
ghost method lemma_mp_incomplete_cycle(N: int, M: int, S: int)
   requires 0 < M < N;
   requires 0 <= S < gcd(N, M);
   ensures forall p, q :: 0 <= p < q < tau(N, M) ==> mp(N, M, S, p) != mp(N, M, S, q);

// The union of all cycles with starts in [0..gcd(N, M)) covers all indexes [0..N)
ghost method lemma_complete_rotation(N: int, M: int, D: set<int>)
   requires 0 < M < N;
   requires forall i, s :: 0 <= i < tau(N, M) && 0 <= s < gcd(N, M) ==> mp(N, M, s, i) in D;
   ensures forall i :: 0 <= i < N ==> i in D;

ghost method lemma_lesseq(X: int, Y: int, F: int)
   requires X <= Y;
   requires 0 <= F;
   ensures X * F <= Y * F;

ghost method lemma_rotmp(A: seq<int>, r: int, S: int, K: int)
   requires 0 < r < |A|;
   requires 0 <= S < gcd(|A|, |A| - r);
   requires 0 < K <= tau(|A|, |A| - r);
   ensures rot(A, r)[mp(|A|, |A| - r, S, K)] == A[mp(|A|, |A| - r, S, K - 1)];

ghost method lemma_inverse_tau_gcd(N: int, M: int, S: int)
   requires 0 < M < N;
   requires 0 <= S;
   ensures S * tau(N, M) < N ==> S < gcd(N, M);



// Left-rotate a by r by modular visit of its elements
method rotate_modulo(a: array<int>, r: int)
   requires a != null;
   requires 0 <= r < a.Length;
   modifies a;
   ensures a.Length == old(a.Length);
   ensures a[..] == rot(old(a[..]), r);
{
   if r == 0 {
      rotate_zero(old(a[..]));
      return;
   }

   var start: int, v: int, moved: int; 
   var displaced: int;

   ghost var done := {};

   start := 0;
   moved := 0;
   while moved != a.Length
   invariant 0 <= moved <= a.Length;
   invariant 0 <= start <= gcd(a.Length, a.Length - r);
   invariant moved < a.Length ==> start < gcd(a.Length, a.Length - r);
   invariant moved == start * tau(a.Length, a.Length - r);
   invariant forall i, s :: 
      0 <= i <= tau(a.Length, a.Length - r) && start <= s < gcd(a.Length, a.Length - r)
      ==> mp(a.Length, a.Length - r, s, i) !in done;
   invariant forall i, s :: 
      0 <= i < tau(a.Length, a.Length - r) && 0 <= s < start
      ==> mp(a.Length, a.Length - r, s, i) in done;
   invariant forall i :: 0 <= i < a.Length && i !in done ==> a[i] == old(a[..])[i];
   invariant forall i :: 0 <= i < a.Length && i in done ==> a[i] == rot(old(a[..]), r)[i];
   {

      v, displaced := start, a[start];
      ghost var k := 0;

      // one unconditional iteration of the inner loop
      k := k + 1;
      v := v + a.Length - r;
      // wrap over a's bounds
      if v >= a.Length { v := v - a.Length; }
      // swap a[v] and displaced
      a[v], displaced := displaced, a[v];
      moved := moved + 1;
      done := done + { v };

      lemma_mp_disjoint_cycles(a.Length, a.Length - r, start, k);
      lemma_mp_incomplete_cycle(a.Length, a.Length - r, start);
      lemma_rotmp(old(a[..]), r, start, k);

      while v != start
      invariant k == moved - start * tau(a.Length, a.Length - r);
      invariant 0 <= v < a.Length;
      invariant 0 < k <= tau(a.Length, a.Length - r);
      invariant v == mp(a.Length, a.Length - r, start, k);
      invariant v != start <==> k < tau(a.Length, a.Length - r);
      invariant forall i, s :: 
         0 <= i < tau(a.Length, a.Length - r) && start < s < gcd(a.Length, a.Length - r)
         ==> mp(a.Length, a.Length - r, s, i) !in done;
      invariant forall i, s :: 
         0 <= i < tau(a.Length, a.Length - r) && 0 <= s < start
         ==> mp(a.Length, a.Length - r, s, i) in done;
      invariant forall i :: 
         0 < i <= k ==> mp(a.Length, a.Length - r, start, i) in done;
      invariant forall i :: 
         k < i <= tau(a.Length, a.Length - r) ==> mp(a.Length, a.Length - r, start, i) !in done;
      invariant displaced == old(a[..])[v];
      invariant forall i :: 0 <= i < a.Length && i !in done ==> a[i] == old(a[..])[i];
      invariant forall i :: 0 <= i < a.Length && i in done ==> a[i] == rot(old(a[..]), r)[i];
      decreases tau(a.Length, a.Length - r) - k;
      {
         k := k + 1;
         v := v + a.Length - r;
         // wrap over a's bounds
         if v >= a.Length { v := v - a.Length; }
         // swap a[v] and displaced
         a[v], displaced := displaced, a[v];
         moved := moved + 1;
         done := done + { v };

         lemma_mp_disjoint_cycles(a.Length, a.Length - r, start, k);
         lemma_mp_complete_cycle(a.Length, a.Length - r, start);
         lemma_mp_incomplete_cycle(a.Length, a.Length - r, start);
         lemma_rotmp(old(a[..]), r, start, k);
      }

      start := start + 1;

      lemma_lesseq(start, gcd(a.Length, a.Length - r), tau(a.Length, a.Length - r));
      lemma_inverse_tau_gcd(a.Length, a.Length - r, start);
      
      forall (s | start <= s < gcd(a.Length, a.Length - r)) {
         lemma_mp_complete_cycle(a.Length, a.Length - r, s);
      }

      calc ==> {
         forall i, s :: 
         0 < i <= tau(a.Length, a.Length - r) && start <= s < gcd(a.Length, a.Length - r) 
         ==> mp(a.Length, a.Length - r, s, i) !in done;
         { forall (s | start <= s < gcd(a.Length, a.Length - r)) {
            lemma_mp_complete_cycle(a.Length, a.Length - r, s);
         }}
         forall i, s :: 
         0 <= i < tau(a.Length, a.Length - r) && start <= s < gcd(a.Length, a.Length - r) 
         ==> mp(a.Length, a.Length - r, s, i) !in done;
      }
   }

   lemma_complete_rotation(a.Length, a.Length - r, done);
}
