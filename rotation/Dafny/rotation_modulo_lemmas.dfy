// Copyright 2014 Carlo A. Furia


module Abstract_ModularRotation
{

   // \rho^r rotation function
   static function rot(S: seq<int>, r: int): seq<int>
      requires 0 <= r < |S|;
      ensures |S| == |rot(S, r)|;

   // rotation by zero is the identity
   static ghost method rotate_zero(S: seq<int>)
      requires 0 < |S|;
      ensures rot(S, 0) == S;

   static function gcd(x: int, y: int): int
      requires 0 < x && 0 < y;
      ensures 0 < gcd(x, y) <= x && gcd(x, y) <= y;

   static function tau(x: int, y: int): int
      requires 0 < x && 0 < y;
      ensures gcd(x, y) * tau(x, y) == x;
      ensures 0 < tau(x, y) <= x;

   static function mp(N: int, M: int, S: int, p: int): int
      requires 0 < M < N;
      requires 0 <= S < N;
      requires 0 <= p;
      ensures 0 <= mp(N, M, S, p) < N;
   // {
   //    if p == 0 then S else 
   //       (if mp(N, M, S, p - 1) + M < N then mp(N, M, S, p - 1) + M 
   //                                      else mp(N, M, S, p - 1) + M - N)
   // }     // Cannot redefine implementations in module refinements

   // Elements of cycles with different starts S are disjoint
   static ghost method lemma_mp_disjoint_cycles(N: int, M: int, S: int, K: int)
      requires 0 < M < N;
      requires 0 <= S < gcd(N, M);
      requires 0 <= K;
      ensures forall t, q :: 0 <= q < tau(N, M) && S < t < gcd(N, M) 
         ==> mp(N, M, S, K) != mp(N, M, t, q);

   // Any cycle goes back to start S after tau(N, M)
   static ghost method lemma_mp_complete_cycle(N: int, M: int, S: int)
      requires 0 < M < N;
      requires 0 <= S < gcd(N, M);
      ensures mp(N, M, S, 0) == mp(N, M, S, tau(N, M));

   // tau(N, M) elements in each cycle are all different
   static ghost method lemma_mp_incomplete_cycle(N: int, M: int, S: int)
      requires 0 < M < N;
      requires 0 <= S < gcd(N, M);
      ensures forall p, q :: 0 <= p < q < tau(N, M) ==> mp(N, M, S, p) != mp(N, M, S, q);

   // The union of all cycles with starts in [0..gcd(N, M)) covers all indexes [0..N)
   static ghost method lemma_complete_rotation(N: int, M: int, D: set<int>)
      requires 0 < M < N;
      requires forall i, s :: 0 <= i < tau(N, M) && 0 <= s < gcd(N, M) ==> mp(N, M, s, i) in D;
      ensures forall i :: 0 <= i < N ==> i in D;

   static ghost method lemma_lesseq(X: int, Y: int, F: int)
      requires X <= Y;
      requires 0 <= F;
      ensures X * F <= Y * F;

   static ghost method lemma_rotmp(A: seq<int>, r: int, S: int, K: int)
      requires 0 < r < |A|;
      requires 0 <= S < gcd(|A|, |A| - r);
      requires 0 < K <= tau(|A|, |A| - r);
      ensures rot(A, r)[mp(|A|, |A| - r, S, K)] == A[mp(|A|, |A| - r, S, K - 1)];

   static ghost method lemma_inverse_tau_gcd(N: int, M: int, S: int)
      requires 0 < M < N;
      requires 0 <= S;
      ensures S * tau(N, M) < N ==> S < gcd(N, M);

}



module ModularRotation refines Abstract_ModularRotation
{

   static function wrap(x: int, y: int): int
      requires 0 <= x && 0 < y;
   {
      if x < y then x
               else x - y
   }

   // \rho^r rotation function
   static function rot(S: seq<int>, r: int): seq<int>
      ensures forall k :: 0 <= k < |S| ==> rot(S, r)[k] == S[wrap(k + r, |S|)];
      ensures forall k :: 0 <= k < |S| ==> S[k] == rot(S, r)[wrap(k + |S| - r, |S|)];
   {
      S[r..|S|] + S[0..r]
   }

   // rotation by zero is the identity
   static ghost method rotate_zero(S: seq<int>)
      ensures rot(S, 0) == S;
   {
   }


   // Modular definitions

   static function gcd(x: int, y: int): int
      ensures x % gcd(x, y) == 0 && y % gcd(x, y) == 0;
      ensures forall z :: 1 < z && x % z == 0 && y % z == 0 ==> z <= gcd(x, y);

   static function tau(x: int, y: int): int
   {
      x / gcd(x, y)
   }

   static function mp(N: int, M: int, S: int, p: int): int
   {
      if p == 0 then S else wrap(mp(N, M, S, p - 1) + M, N)
   }


   // Basic (axiomatic) properties (which could in principle be proved from definitions)

   // Elements of cycles with different starts S are disjoint
   static ghost method lemma_mp_disjoint_cycles(N: int, M: int, S: int, K: int)

   // Any cycle goes back to start S after tau(N, M)
   static ghost method lemma_mp_complete_cycle(N: int, M: int, S: int)

   // tau(N, M) elements in each cycle are all different
   static ghost method lemma_mp_incomplete_cycle(N: int, M: int, S: int)

   // The union of all cycles with starts in [0..gcd(N, M)) covers all indexes [0..N)
   static ghost method lemma_complete_rotation(N: int, M: int, D: set<int>)


   // Derived (lemma) properties

   static ghost method lemma_lesseq(X: int, Y: int, F: int)
   {
   }

   static ghost method lemma_rotmp(A: seq<int>, r: int, S: int, K: int)
   {
   }

   static ghost method lemma_inverse_tau_gcd(N: int, M: int, S: int)
   {
   }

}
