// Copyright 2014 Carlo A. Furia


// Commenting out (or removing) unused definitions of wrap and rot's postconditions
// is necessary to have converging performance

// \rho^r rotation function
function rot(S: seq<int>, r: int): seq<int>
   requires 0 <= r < |S|;
   ensures |S| == |rot(S, r)|;
//   ensures forall k :: 0 <= k < |S| ==> rot(S, r)[k] == S[wrap(k + r, |S|)];
//   ensures forall k :: 0 <= k < |S| ==> S[k] == rot(S, r)[wrap(k + |S| - r, |S|)];
{
   S[r..|S|] + S[0..r]
}


ghost method left_is_smaller(X: seq<int>, Y: seq<int>, Z: seq<int>, d: int)
   requires |X| == |Z| == d;
   requires |Y| > 0;
   ensures rot(X + Y + Z, d) == rot(Z + Y, d) + X;
{
}

ghost method right_is_smaller(X: seq<int>, Y: seq<int>, Z: seq<int>, d: int)
   requires 0 < |X| == |Z| == d;
   requires |Y| > 0;
   ensures |X| + |Y| + |Z| - 2*d < |X| + |Y| + |Z| - d;
   ensures rot(X + Y + Z, |X| + |Y| + |Z| - d) == Z + rot(Y + X, |X| + |Y| + |Z| - 2*d);
{
}


// swap a[low..low + d) and a[high - d..high)
method swap_sections(a: array<int>, low: int, high: int, d: int)
   requires a != null;
   requires 0 <= low <= low + d <= high - d <= high <= a.Length;
   modifies a;
   ensures a[low..low + d] == old(a[high - d.. high]);
   ensures a[low + d..high - d] == old(a[low + d..high - d]);
   ensures a[high - d..high] == old(a[low..low + d]);
      // redundant with the previous 3, but useful all around
   ensures a[low..high] == old(a[high - d.. high]) + old(a[low + d..high - d]) + old(a[low..low + d]);
   ensures forall i :: 0 <= i < low ==> a[i] == old(a[i]);
   ensures forall i :: high <= i < a.Length ==> a[i] == old(a[i]);
{
   var x: int, z: int;

   x, z := low, high - d;
   while x < low + d
   invariant low <= x <= low + d;
   invariant high - d <= z <= high;
   invariant x - low == z - (high - d);
      // explicit quantification works much better here
   invariant forall i :: low <= i < x ==> a[i] == old(a[..])[high - d + i - low];
   invariant forall i :: x <= i < high - d ==> a[i] == old(a[..])[i];
   invariant forall i :: high - d <= i < z ==> a[i] == old(a[..])[low + i - (high - d)];
   invariant forall i :: z <= i < high ==> a[i] == old(a[..])[i];
   // frame invariants
   invariant forall i :: 0 <= i < low ==> a[i] == old(a[i]);
   invariant forall i :: high <= i < a.Length ==> a[i] == old(a[i]);
   {
      // swap a[x] and a[z]
      a[x], a[z] := a[z], a[x];
      x, z := x + 1, z + 1;
   }
}


// Rotate a[low..p..high) at p by swapping recursively 
method rotate_swap_helper(a: array<int>, low: int, p: int, high: int)
   requires a != null;
   requires 0 <= low <= p < high <= a.Length;
   modifies a;
   decreases high - low;
   ensures a[low..high] == rot(old(a[low..high]), p - low);
   ensures forall i :: 0 <= i < low ==> a[i] == old(a[i]);
   ensures forall i :: high <= i < a.Length ==> a[i] == old(a[i]);
{
   if low < p < high
   {
      if p - low == high - p {
         // swap a[low..p) and a[p..high)
         swap_sections(a, low, high, p - low);
      } else { 
      if p - low < high - p {
         // swap a[low..p) and a[high - (p - low)..high]
         swap_sections(a, low, high, p - low);
         assert a[low..high - (p - low)] == old(a[high - (p - low)..high]) + old(a[p..high - (p - low)]);
         rotate_swap_helper(a, low, p, high - (p - low));
         left_is_smaller(old(a[low..p]), old(a[p..high - (p - low)]), old(a[high - (p - low)..high]), p - low);
         assert old(a[low..high]) == 
                old(a[low..p]) + old(a[p..high - (p - low)]) + old(a[high - (p - low)..high]);
      } else {
      if p - low > high - p {
         // swap a[low..low + (high - p)) and a[p..high)
         swap_sections(a, low, high, high - p);
         assert a[low + (high - p)..high] == old(a[low + (high - p)..p]) + old(a[low..low + (high - p)]);
         rotate_swap_helper(a, low + (high - p), p, high);
         right_is_smaller(old(a[low..low + (high - p)]), old(a[low + (high - p)..p]), old(a[p..high]), high - p);
         assert old(a[low..high]) == 
                old(a[low..low + (high - p)]) + old(a[low + (high - p)..p]) + old(a[p..high]);
           // another sequence decomposition needed to related rotations by N - d and d
         assert a[low..high] == 
                a[low..low + (high - p)] + a[low + (high - p)..low + 2*(high - p)] + a[low + 2*(high - p)..high];
         assert a[low..low + (high - p)] == old(a[p..high]);
      }}}
   }
}


// Left-rotate a by r by swapping equal segments 
method rotate_swap(a: array<int>, r: int)
   requires a != null;
   requires 0 <= r < a.Length;
   modifies a;
   ensures a.Length == old(a.Length);
   ensures a[..] == rot(old(a[..]), r);
{
   assert a[..] == a[0..a.Length];
   rotate_swap_helper(a, 0, r, a.Length);
}
