// Copyright 2015 Carlo A. Furia


public class RotationSwapIterative {

    /*@
      @ requires 0 <= x && 0 < y;
      @
      @ ensures 0 <= \result && \result < y;
      @ ensures x < y ==> \result == x;
      @ ensures y <= x && x < 2*y ==> \result == x - y;
      @*/
    public static /*@ pure @*/ int wrap(int x, int y)
    {
        if (x < y) {
            return x;
        } else {
            return wrap(x - y, y);
        }
    }


    /*@
      @ requires 0 <= r && r < N;
      @ requires 0 <= k && k < N;
      @
      @ ensures 0 <= \result && \result < N;
      @ ensures \result == wrap(k + N - r, N);
      @*/
    // position k maps to in a rotation of N-element sequence by r
    public static /*@ pure @*/ int rotp(int k, int N, int r)
    {
        return wrap(k + N - r, N);
    }


    /*@
      @ requires low <= p && p < high;
      @ requires low <= k && k < high;
      @
      @ ensures low <= \result && \result < high;
      @ ensures \result == low + wrap(k - low + (high - low) - (p - low), high - low);
      @ ensures \result == low + rotp(k - low, high - low, p - low);
      @*/
    // position k maps to in a rotation of [low..high) at p
    public static /*@ pure @*/ int rlh(int k, int low, int high, int p)
    {
        return low + wrap(k + (high - low) - p, high - low);
    }


    /*@
      @ requires low <= p && p < high;
      @ requires low <= k && k < high;
      @
      @ ensures low <= \result && \result < high;
      @ ensures \result == low + wrap((k - low) + (p - low), high - low);
      @*/
    // position that maps to k in a rotation of [low..high) at p
    // llh is the inverse of rlh:  llh(rlh(k, low, high, p), low, high, p) = k
    public static /*@ pure @*/ int llh(int k, int low, int high, int p)
    {
        return low + wrap((k - low) + (p - low), high - low);
    }


    /*@
      @ requires a != null;
      @ requires 0 <= low && low <= low + d && low + d <= high - d && high - d <= high && high <= a.length;
      @
      @ ensures (\forall int i; low <= i && i < low + d ==> a[i] == \old(a[high - d + i - low]));
      @ ensures (\forall int i; low + d <= i && i < high - d ==> a[i] == \old(a[i]));  
      @ ensures (\forall int i; high - d <= i && i < high ==> a[i] == \old(a[low + i - (high - d)]));
      @ ensures (\forall int i; 0 <= i && i < low ==> a[i] == \old(a[i]));
      @ ensures (\forall int i; high <= i && i < a.length ==> a[i] == \old(a[i]));
      @*/
    public static void swap_sections(int[] a, int low, int high, int d)
    {
        int x = low, z = high - d;

        //@ loop_invariant low <= x && x <= low + d;
        //@ loop_invariant high - d <= z && z <= high;
        //@ loop_invariant x - low == z - (high - d);
        //@ loop_invariant (\forall int i; low <= i && i < x ==> a[i] == \old(a[high - d + i - low]));
        //@ loop_invariant (\forall int i; x <= i && i < high - d ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; high - d <= i && i < z ==> a[i] == \old(a[low + i - (high - d)]));
        //@ loop_invariant (\forall int i; z <= i && i < high ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; 0 <= i && i < low ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; high <= i && i < a.length ==> a[i] == \old(a[i]));
        while (x < low + d)
        {
            int tmp = a[x];
            a[x] = a[z];
            a[z] = tmp;
            x++;
            z++;
        }
    }


    /*@
      @ requires a != null;
      @ requires 0 <= r && r < a.length;
      @
      @ ensures (\forall int i; 0 <= i && i < a.length ==> a[rotp(i, a.length, r)] == \old(a[i]));
      @*/
    public static void rotate_swap_iterative(int[] a, int r)
    {

        if (r == 0) return;

        int low, high, p;

        low = 0; p = r; high = a.length;

        //@ loop_invariant 0 <= low && low <= p && p <= high && high <= a.length;
        //@ loop_invariant low == p <==> p == high;
        //@ loop_invariant (\forall int i; 0 <= i && i < low ==> a[i] == \old(a[llh(i, 0, a.length, p)]));
        //@ loop_invariant (\forall int i; high <= i && i < a.length ==> a[i] == \old(a[llh(i, 0, a.length, p)]));
        //@ loop_invariant (\forall int i; low <= i && i < high ==> a[i] == \old(a[i]));
        //@ loop_invariant p - low < high - low ==> (\forall int i; low <= i && i < high ==> \old(a[llh(i, 0, a.length, p)]) == a[llh(i, low, high, p)]);
        while (low < p && p < high)
        {
            if (p - low == high - p) {
                swap_sections(a, low, high, p - low);
                low = low + (p - low);
                high = high - (high - p);
            }
            else if (p - low < high - p) {
                swap_sections(a, low, high, p - low);
                high = high - (p - low);
            }
            else if (p - low > high - p) {
                swap_sections(a, low, high, high - p);
                low = low + (high - p);
            }
        }
    }

}
