This directory contains Java files of JML-annotated implementations of
various algorithms to rotate arrays, which can be verified using
ESC/Java2. I describe in detail the algorithms and their correctness
proofs in the following technical report:

    http://arxiv.org/abs/1406.5453

The directory also contains a Makefile to run ESC/Java2 on the
annotated inputs.

These files were developed with ESC/Java2 3049 (release 22
Oct. 2007), and Simplify 1.5.4.

All files in this directory are authored by Carlo A. Furia
(http://bugcounting.net).

The files in this directory are free software: you can redistribute
them and/or modify them under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version. A copy of the
GNU General Public License is available in the file COPYING in this
directory.

The files in this directory are distributed in the hope that they will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.
