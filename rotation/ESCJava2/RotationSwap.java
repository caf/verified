// Copyright 2015 Carlo A. Furia


public class RotationSwap {

    /*@
      @ requires 0 <= x && 0 < y;
      @
      @ ensures 0 <= \result && \result < y;
      @ ensures x < y ==> \result == x;
      @ ensures y <= x && x < 2*y ==> \result == x - y;
      @*/
    public static /*@ pure @*/ int wrap(int x, int y)
    {
        if (x < y) {
            return x;
        } else {
            return wrap(x - y, y);
        }
    }


    /*@
      @ requires 0 <= r && r < N;
      @ requires 0 <= k && k < N;
      @
      @ ensures 0 <= \result && \result < N;
      @ ensures \result == wrap(k + N - r, N);
      @*/
    // position k maps to in a rotation of N-element sequence by r
    public static /*@ pure @*/ int rotp(int k, int N, int r)
    {
        return wrap(k + N - r, N);
    }


    /*@
      @ requires low <= p && p < high;
      @ requires low <= k && k < high;
      @
      @ ensures low <= \result && \result < high;
      @ ensures \result == low + wrap(k - low + (high - low) - (p - low), high - low);
      @ ensures \result == low + rotp(k - low, high - low, p - low);
      @*/
    // position k maps to in a rotation of [low..high) at p
    public static /*@ pure @*/ int rlh(int k, int low, int high, int p)
    {
        return low + wrap(k + (high - low) - p, high - low);
    }


    /*@
      @ requires a != null;
      @ requires 0 <= low && low <= low + d && low + d <= high - d && high - d <= high && high <= a.length;
      @
      @ ensures (\forall int i; low <= i && i < low + d ==> a[i] == \old(a[high - d + i - low]));
      @ ensures (\forall int i; low + d <= i && i < high - d ==> a[i] == \old(a[i]));  
      @ ensures (\forall int i; high - d <= i && i < high ==> a[i] == \old(a[low + i - (high - d)]));
      @ ensures (\forall int i; 0 <= i && i < low ==> a[i] == \old(a[i]));
      @ ensures (\forall int i; high <= i && i < a.length ==> a[i] == \old(a[i]));
      @*/
    public static void swap_sections(int[] a, int low, int high, int d)
    {
        int x = low, z = high - d;

        //@ loop_invariant low <= x && x <= low + d;
        //@ loop_invariant high - d <= z && z <= high;
        //@ loop_invariant x - low == z - (high - d);
        //@ loop_invariant (\forall int i; low <= i && i < x ==> a[i] == \old(a[high - d + i - low]));
        //@ loop_invariant (\forall int i; x <= i && i < high - d ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; high - d <= i && i < z ==> a[i] == \old(a[low + i - (high - d)]));
        //@ loop_invariant (\forall int i; z <= i && i < high ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; 0 <= i && i < low ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; high <= i && i < a.length ==> a[i] == \old(a[i]));
        while (x < low + d)
        {
            int tmp = a[x];
            a[x] = a[z];
            a[z] = tmp;
            x++;
            z++;
        }
    }


    /*@
      @ requires 0 <= low && low < p && p < high && high <= A.length;
      @ requires p - low < high - p;
      @          // X in place
      @ ensures (\forall int i; low <= i && i < p ==> rlh(i, low, high, p) == i + (high - p));
      @          // Z after sub-rotation
      @ ensures (\forall int i; high - (p - low) <= i && i < high ==> 
      @                         rlh(i, low, high, p) == rlh(i - (high - p), low, high - (p - low), p));
      @         // Y after sub-rotation
      @ ensures (\forall int i; p <= i && i < high - (p - low) ==> 
      @                         rlh(i, low, high, p) == rlh(i, low, high - (p - low), p));
      @*/
    public static /*@ pure @*/ boolean lemma_left(int[] A, int low, int p, int high)
    {
    }


    /*@
      @ requires 0 <= low && low < p && p < high && high <= A.length;
      @ requires p - low > high - p;
      @          // Z in place
      @ ensures (\forall int i; p <= i && i < high ==> rlh(i, low, high, p) == i - (p - low));
      @          // X after sub-rotation
      @ ensures (\forall int i; low <= i && i < low + (high - p) ==>
      @                         rlh(i, low, high, p) == rlh(i + (p - low), low + (high - p), high, p));
      @          // Y after sub-rotation
      @ ensures (\forall int i; low + (high - p) <= i && i < p ==>
      @                         rlh(i, low, high, p) == rlh(i, low + (high - p), high, p));
      @*/
    public static /*@ pure @*/ boolean lemma_right(int[] A, int low, int p, int high)
    {
    }


    /*@
      @ requires a != null;
      @ requires 0 <= low && low <= p && p < high && high <= a.length;
      @
      @ ensures (\forall int i; low <= i && i < high ==> a[rlh(i, low, high, p)] == \old(a[i]));
      @*/
    public static void rotate_swap_helper(int[] a, int low, int p, int high)
    {
        if (low < p && p < high) {
            if (p - low == high - p) {
                swap_sections(a, low, high, p - low);
            } 
            else if (p - low < high - p) {
                swap_sections(a, low, high, p - low);
                rotate_swap_helper(a, low, p, high - (p - low));
                lemma_left(a, low, p, high);
            }
            else if (p - low > high - p) {
                swap_sections(a, low, high, high - p);
                rotate_swap_helper(a, low + (high - p), p, high);
                lemma_right(a, low, p, high);
            }
        }
    }


    /*@
      @ requires a != null;
      @ requires 0 <= r && r < a.length;
      @
      @ ensures (\forall int i; 0 <= i && i < a.length ==> a[rotp(i, a.length, r)] == \old(a[i]));
      @*/
    public static void rotate_swap(int[] a, int r)
    {
        rotate_swap_helper(a, 0, r, a.length);
        //@ assert (\forall int i; 0 <= i && i < a.length ==> a[rlh(i, 0, a.length, r)] == \old(a[i]));
        //@ assert (\forall int i; 0 <= i && i < a.length ==> rlh(i, 0, a.length, r) == rotp(i, a.length, r));
        //@ assert (\forall int i; 0 <= i && i < a.length ==> a[rlh(i, 0, a.length, r)] == a[rotp(i, a.length, r)]);
        /*@ assert 
          (\forall int i; 0 <= i && i < a.length ==> a[rlh(i, 0, a.length, r)] == a[rotp(i, a.length, r)]) &&
          (\forall int i; 0 <= i && i < a.length ==> a[rlh(i, 0, a.length, r)] == \old(a[i]))
          ==>
          (\forall int i; 0 <= i && i < a.length ==> a[rotp(i, a.length, r)] == \old(a[i]));
          @*/
        // ESC/Java2 can prove the implication but cannot apply it: just assuming the consequent
        //@ assume (\forall int i; 0 <= i && i < a.length ==> a[rotp(i, a.length, r)] == \old(a[i]));
    }

}
