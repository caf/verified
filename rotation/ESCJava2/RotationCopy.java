// Copyright 2015 Carlo A. Furia


public class RotationCopy {

    /*@
      @ requires 0 <= x && 0 < y;
      @
      @ ensures 0 <= \result && \result < y;
      @ ensures x < y ==> \result == x;
      @ ensures y <= x && x < 2*y ==> \result == x - y;
      @*/
    public static /*@ pure @*/ int wrap(int x, int y)
    {
        if (x < y) {
            return x;
        } else {
            return wrap(x - y, y);
        }
    }


    /*@
      @ requires 0 <= r && r < N;
      @ requires 0 <= k && k < N;
      @
      @ ensures 0 <= \result && \result < N;
      @ ensures \result == wrap(k + N - r, N);
      @*/
    // position k maps to in a rotation of N-element sequence by r
    public static /*@ pure @*/ int rotp(int k, int N, int r)
    {
        return wrap(k + N - r, N);
    }


    /*@
      @ requires S != null && T != null && S != T;
      @ requires 0 <= r && r < S.length && S.length == T.length;
      @
      @ ensures \result <==> (\forall int i; 0 <= i && i < S.length ==> T[rotp(i, S.length, r)] == S[i]);
      @*/
    // Is T a rotation of S by r?
    public static /*@ pure @*/ boolean is_rot(int[] S, int[] T, int r)
    {
        //@ loop_invariant 0 <= k && k <= S.length;
        //@ loop_invariant (\forall int i; 0 <= i && i < k ==> T[rotp(i, S.length, r)] == S[i]);
        for (int k = 0; k < S.length; k++) {
            if (S[k] != T[rotp(k, S.length, r)])
                return false;
        }
        return true;
    }


    /*@
      @ requires to != null && from != null && to != from;
      @ requires to.length == from.length;
      @
      @ ensures (\forall int i; 0 <= i && i < from.length ==> from[i] == \old(from[i]));
      @ ensures (\forall int i; 0 <= i && i < from.length ==> from[i] == to[i]);
      @*/
    public static void copy(int[] to, int[] from)
    {
        //@ loop_invariant 0 <= k && k <= to.length;
        //@ loop_invariant (\forall int i; 0 <= i && i < from.length ==> from[i] == \old(from[i]));
        //@ loop_invariant (\forall int i; 0 <= i && i < k ==> from[i] == to[i]);
        for (int k = 0; k < to.length; k++)
        {
            to[k] = from[k];
        }
    }


    /*@
      @ requires a != null;
      @ requires 0 <= r && r < a.length;
      @
      @ ensures (\forall int i; 0 <= i && i < a.length ==> a[rotp(i, a.length, r)] == \old(a[i]));
      // cannot use "is_rot" directly since I'd need to save \old(a[..]), not just \old(a) == a;
      @*/
    public static void rotate_copy(int[] a, int r)
    {
        if (r == 0)
            return;

        int[] b = new int[a.length];
        int s  = 0, d = a.length - r;

        //@ loop_invariant 0 <= s && s <= a.length;
        //@ loop_invariant d == wrap(s + a.length - r, a.length);
        //@ loop_invariant (\forall int i; 0 <= i && i < a.length ==> a[i] == \old(a[i]));
        //@ loop_invariant (\forall int i; 0 <= i && i < s ==> a[i] == b[wrap(i + a.length - r, a.length)]);
        while (s < a.length) {
            b[d] = a[s];
            s++;
            d++;
            // wrap over a's bounds
            if (d == a.length)
                d = 0;
        }
        // copy b's content back into a
        copy(b, a);
    }

}
