// Copyright 2015 Carlo A. Furia


public class RotationReverse {

    /*@
      @ requires 0 <= x && 0 < y;
      @
      @ ensures 0 <= \result && \result < y;
      @ ensures x < y ==> \result == x;
      @ ensures y <= x && x < 2*y ==> \result == x - y;
      @*/
    public static /*@ pure @*/ int wrap(int x, int y)
    {
        if (x < y) {
            return x;
        } else {
            return wrap(x - y, y);
        }
    }


    /*@
      @ requires 0 <= x && x <= y && y <= z && z <= A.length;
      @
      @ ensures (\forall int i; x <= i && i < y ==> rp(i, x, z) == rp(i, x, y) + z - y);
      @ ensures (\forall int i; y <= i && i < z ==> rp(i, x, z) == rp(i, y, z) - y + x);
      @*/
    public static /*@ pure @*/ void lemma_rev_cat(int[] A, int x, int y, int z)
    {
    }


    /*@
      @ requires 0 <= x && x <= z && z <= A.length;
      @
      @ ensures (\forall int i; x <= i && i < z ==> rp(rp(i, x, z), x, z) == i);
      @*/
    public static /*@ pure @*/ void lemma_rev_rev(int[] A, int x, int z)
    {
    }


    /*@
      @ requires 0 <= r && r < N;
      @ requires 0 <= k && k < N;
      @
      @ ensures 0 <= \result && \result < N;
      @ ensures \result == wrap(k + N - r, N);
      @*/
    // position k maps to in a rotation of N-element sequence by r
    public static /*@ pure @*/ int rotp(int k, int N, int r)
    {
        return wrap(k + N - r, N);
    }


    /*@
      @ requires low <= k && k < high;
      @
      @ ensures low <= \result && \result < high;
      @ ensures \result == high + low - 1 - k;
      @*/
    // position k maps to in a reversal of [low..high)
    public static /*@ pure @*/ int rp(int k, int low, int high)
    {
        return high + low - 1 - k;
    }


    /*@
      @ requires a != null;
      @ requires 0 <= low && low <= high && high <= a.length;
      @
      @ ensures (\forall int i; low <= i && i < high ==> a[rp(i, low, high)] == \old(a[i]));
      @ ensures (\forall int i; 0 <= i && i < low ==> a[i] == \old(a[i]));
      @ ensures (\forall int i; high <= i && i < a.length ==> a[i] == \old(a[i]));
      @*/
    public static void reverse(int[] a, int low, int high)
    {
        int p = low, q = high - 1;

        //@ loop_invariant low <= p && p <= q + 2 && q + 2 <= high + 1;
        //@ loop_invariant q == high + low - 1 - p;
        //@ loop_invariant (\forall int i; low <= i && i < p ==> \old(a[i]) == a[rp(i, low, high)]);
        //@ loop_invariant (\forall int i; q < i && i < high ==> \old(a[i]) == a[rp(i, low, high)]);
        //@ loop_invariant (\forall int i; p <= i && i <= q ==> \old(a[i]) == a[i]);
        //@ loop_invariant (\forall int i; 0 <= i && i < low ==> \old(a[i]) == a[i]);
        //@ loop_invariant (\forall int i; high <= i && i < a.length ==> \old(a[i]) == a[i]);
        while (p < q + 1)
        {
            int tmp = a[p];
            a[p] = a[q];
            a[q] = tmp;
            p++;
            q--;
        }
    }


    /*@
      @ requires to != null && from != null && to != from;
      @ requires to.length == from.length;
      @
      @ ensures (\forall int i; 0 <= i && i < from.length ==> from[i] == \old(from[i]));
      @ ensures (\forall int i; 0 <= i && i < from.length ==> from[i] == to[i]);
      @*/
    public static void copy(int[] to, int[] from)
    {
        //@ loop_invariant 0 <= k && k <= to.length;
        //@ loop_invariant (\forall int i; 0 <= i && i < from.length ==> from[i] == \old(from[i]));
        //@ loop_invariant (\forall int i; 0 <= i && i < k ==> from[i] == to[i]);
        for (int k = 0; k < to.length; k++)
        {
            to[k] = from[k];
        }
    }


    /*@
      @ requires a != null;
      @ requires 0 <= r && r < a.length;
      @
      @ ensures (\forall int i; 0 <= i && i < a.length ==> a[rotp(i, a.length, r)] == \old(a[i]));
      @*/
    public static void rotate_reverse(int[] a, int r)
    {
        int[] b = new int[a.length];  // ghost

        reverse(a, 0, r);
        reverse(a, r, a.length);
        copy(b, a);  // ghost
        lemma_rev_cat(a, 0, r, a.length);
        reverse(a, 0, a.length);
        lemma_rev_rev(a, 0, r);
        lemma_rev_rev(a, r, a.length);
    }

}
