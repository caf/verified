// Copyright 2015 Carlo A. Furia


public class RotationModulo {

	 /*@
      @ requires 0 <= x && 0 < y;
		@
		@ ensures 0 <= \result && \result < y;
      @ ensures x < y ==> \result == x;
		@ ensures y <= x && x < 2*y ==> \result == x - y;
      @*/
	 public static /*@ pure @*/ int wrap(int x, int y)
	 {
		  if (x < y) {
				return x;
		  } else {
				return wrap(x - y, y);
		  }
	 }


	 /*@
      @ requires 0 <= r && r < N;
		@ requires 0 <= k && k < N;
		@
		@ ensures 0 <= \result && \result < N;
		@ ensures \result == wrap(k + N - r, N);
      @*/
	 // position k maps to in a rotation of N-element sequence by r
	 public static /*@ pure @*/ int rotp(int k, int N, int r)
	 {
		  return wrap(k + N - r, N);
	 }


	 /*@
      @ requires 0 <= r && r < N;
		@ requires 0 <= k && k < N;
		@
		@ ensures 0 <= \result && \result < N;
		@ ensures \result == wrap(k + r, N);
      @*/
	 // position that maps to k in a rotation of N-element sequence by r
    // ptor is the inverse of rotp:  ptor(rotp(k, N, r), N, r) == k
	 public static /*@ pure @*/ int ptor(int k, int N, int r)
	 {
		  return wrap(k + r, N);
	 }


	 /*@
      @ requires 0 < x && 0 < y;
		@
		@ ensures 0 < \result && \result <= x && \result <= y;
      @*/
	 public static /*@ pure @*/ int gcd(int x, int y)
	 {
		  if (x == y)
				return x;
		  if (x > y)
				return gcd(x - y, y);
		  else
				return gcd(x, y - x);
	 }


	 /*@
      @ requires 0 < x && 0 < y;
		@
		@ ensures gcd(x, y) * \result == x;
		@ ensures 0 < \result && \result <= x;
      @*/
	 public static /*@ pure @*/ int tau(int x, int y)
	 {
		  //@ assume gcd(x, y) * (x / gcd(x, y)) == x;
		  //@ assert 0 < x / gcd(x, y);
		  return x / gcd(x, y);
	 }


	 /*@
      @ requires 0 < M && M < N;
		@ requires 0 <= S && S < N;
		@ requires 0 <= p;
		@
		@ ensures 0 <= \result && \result < N;
		@ ensures p == 0 ==> \result == S;
		@ ensures p != 0 && mp(N, M, S, p - 1) + M < N ==> \result == mp(N, M, S, p - 1) + M;
		@ ensures p != 0 && mp(N, M, S, p - 1) + M >= N ==> \result == mp(N, M, S, p - 1) + M - N;
      @*/
	 public static /*@ pure @*/ int mp(int N, int M, int S, int p)
	 {
		  if (p == 0)
				return S;
		  return wrap(mp(N, M, S, p - 1) + M, N);
	 }


	 /*@
      @ requires 0 < M && M < N;
		@ requires 0 <= S && S < gcd(N, M);
		@ requires 0 <= K;
		@
		@ ensures (\forall int t, q; 0 <= q && q < tau(N, M) && S < t && t < gcd(N, M)
		@                            ==> mp(N, M, S, K) != mp(N, M, t, q));
      @*/
	 public static /*@ pure @*/ boolean lemma_mp_disjoint_cycles(int N, int M, int S, int K)
	 {
		  /*@ assume (\forall int t, q; 0 <= q && q < tau(N, M) && S < t && t < gcd(N, M) 
			 @         ==> mp(N, M, S, K) != mp(N, M, t, q)); */
	 }


	 /*@
      @ requires 0 < M && M < N;
		@ requires 0 <= S && S < gcd(N, M);
		@
		@ ensures mp(N, M, S, 0) == mp(N, M, S, tau(N, M));
      @*/
	 public static /*@ pure @*/ boolean lemma_mp_complete_cycle(int N, int M, int S)
	 {
		  //@ assume mp(N, M, S, 0) == mp(N, M, S, tau(N, M));
	 }


	 /*@
      @ requires 0 < M && M < N;
		@ requires 0 <= S && S < gcd(N, M);
		@
		@ ensures (\forall int p, q; 0 <= p && p < q && q < tau(N, M) ==> mp(N, M, S, p) != mp(N, M, S, q));
      @*/
	 public static /*@ pure @*/ boolean lemma_mp_incomplete_cycle(int N, int M, int S)
	 {
		  /*@ assume (\forall int p, q; 0 <= p && p < q && q < tau(N, M) 
			 @         ==> mp(N, M, S, p) != mp(N, M, S, q)); */
	 }


	 /*@
      @ requires 0 < r && r < A.length;
		@ requires 0 <= S && S < A.length;
      @ requires K > 0;
		@
		@ ensures rotp(mp(A.length, A.length - r, S, K - 1), A.length, r) == 
		@         mp(A.length, A.length - r, S, K);
      @*/
	 public static /*@ pure @*/ boolean lemma_rotmp(int[] A, int r, int S, int K)
	 {
	 }


	 /*@
      @ requires 0 < r && r < S.length;
		@ requires (\forall int i, s; 
		@           0 <= i && i < tau(S.length, S.length - r) && 0 <= s && s < gcd(S.length, S.length - r)
		@           ==> S[mp(S.length, S.length - r, s, i)]);
		@
		@ ensures (\forall int i; 0 <= i && i < S.length ==> S[i]);
      @*/
	 public static /*@ pure @*/ boolean lemma_onto(boolean[] S, int r)
	 {
		  //@ assume (\forall int i; 0 <= i && i < S.length ==> S[i]);
	 }


	 /*@
      @ requires a != null;
      @ requires 0 <= r && r < a.length;
		@
		@ ensures (\forall int i; 0 <= i && i < a.length ==> a[rotp(i, a.length, r)] == \old(a[i]));
      @*/
    public static void rotate_modulo(int[] a, int r)
	 {
		  if (r == 0) {
				return;
		  }

		  // Framing works better with an explicit 'olda'
		  int olda[] = new int[a.length];
		  //@ loop_invariant 0 <= j && j <= a.length;
		  //@ loop_invariant (\forall int i; 0 <= i && i < a.length ==> a[i] == \old(a[i]));
		  //@ loop_invariant (\forall int i; 0 <= i && i < j ==> olda[i] == \old(a[i]));
		  for (int j = 0; j < a.length; j++)
		  {
				olda[j] = a[j];
		  }
				

		  int start = 0;
		  boolean set[] = new boolean[a.length]; // ghost
		  int k;

		  //@ loop_invariant 0 <= start && start <= gcd(a.length, a.length - r);
		  //@ loop_invariant (\forall int i; 0 <= i && i < a.length && ! set[i] ==> a[i] == olda[i]);
        /*@ loop_invariant (\forall int i, s; 
          @                 0 <= i && i < tau(a.length, a.length - r) &&
          @                 start <= s && s < gcd(a.length, a.length - r)
          @                 ==> ! set[mp(a.length, a.length - r, s, i)]); */
		  /*@ loop_invariant (\forall int i, s; 
          @                 0 <= i && i < tau(a.length, a.length - r) &&
          @                 0 <= s && s < start
          @                 ==> set[mp(a.length, a.length - r, s, i)]); */
   	  /*@ loop_invariant (\forall int i; 0 <= i && i < a.length && set[i] 
          @                 ==> a[i] == olda[ptor(i, a.length, r)]); */
		  while (start < gcd(a.length, a.length - r))
		  {
				int displaced, v;
				displaced = a[start];
				v = start;
				k = 0;

				//@ loop_invariant 0 <= v && v < a.length;
				//@ loop_invariant 0 <= k && k <= tau(a.length, a.length - r);
				//@ loop_invariant v == mp(a.length, a.length - r, start, k);
				//@ loop_invariant 0 < k && v != start <==> 0 < k && k < tau(a.length, a.length - r);
				//@ loop_invariant (\forall int i; 0 <= i && i < a.length && ! set[i] ==> a[i] == olda[i]);
				//@ loop_invariant displaced == olda[v];
				/*@ loop_invariant (\forall int i; k < i && i < tau(a.length, a.length - r)
              @                 ==> ! set[mp(a.length, a.length - r, start, i)]); */
				/*@ loop_invariant (\forall int i; 0 < i && i <= k
              @                 ==> set[mp(a.length, a.length - r, start, i)]); */
            /*@ loop_invariant (\forall int i, s; 
              @                 0 <= i && i < tau(a.length, a.length - r) &&
              @                 start < s && s < gcd(a.length, a.length - r)
              @                 ==> ! set[mp(a.length, a.length - r, s, i)]); */
   		   /*@ loop_invariant (\forall int i, s; 
              @                 0 <= i && i < tau(a.length, a.length - r) &&
              @                 0 <= s && s < start
              @                 ==> set[mp(a.length, a.length - r, s, i)]); */
   		   /*@ loop_invariant (\forall int i; 0 <= i && i < a.length && set[i] 
              @                 ==> a[i] == olda[ptor(i, a.length, r)]); */
				do
				{
					 k = k + 1;
					 v = v + a.length - r;
					 if (v >= a.length) {
						  v = v - a.length;
					 }
					 int tmp = a[v];
					 a[v] = displaced;
					 displaced = tmp;
					 set[v] = true; // ghost
					 lemma_mp_complete_cycle(a.length, a.length - r, start);
					 lemma_mp_incomplete_cycle(a.length, a.length- r, start);
					 lemma_mp_disjoint_cycles(a.length, a.length - r, start, k);
				} while (k < tau(a.length, a.length - r));
				start = start + 1;
        }
		  lemma_onto(set, r);
		  // I couldn't find a way to connect olda[...] to the original content \old(a[...]), 
        // even though it should be obvious that they still are equal. Hence, I assume it.
		  /*@ assume (\forall int i; 0 <= i && i < a.length 
			 @         ==> olda[ptor(i, a.length, r)] == \old(a[ptor(i, a.length, r)])); */

	 }

}
