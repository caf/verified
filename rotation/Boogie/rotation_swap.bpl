// Copyright 2014 Carlo A. Furia


// Rotate a[low..p..high) at p by swapping recursively 
// (An implementation is proved elsewhere)
procedure rotate_swap_helper(a: [int]int, p: int, low, high: int) returns(b: [int]int);
   requires low <= p && p < high;
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: 0 <= i && i < high - low 
           ==>
           rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);


// Left-rotate a by r by swapping equal segments 
// Key correctness argument: if |X| = |Z| = r and r < N - r, 
//                           then rot(X Y Z, r) = rot(Z Y, r) X = Y Z X;
//                           if |X| = |Z| = N - r and r > N - r, 
//                           then rot(X Y Z, r) = Z rot(Y X, r + r - N) = Z X Y.
procedure rotate_swap(a: [int]int, N: int, r: int) returns(b: [int]int)
   requires 0 <= r && r < N;
   ensures (forall i: int :: 0 <= i && i < N ==> seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
{
   call b := rotate_swap_helper(a, r, 0, N);
}
