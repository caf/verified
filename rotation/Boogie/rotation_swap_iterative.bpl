// Copyright 2014 Carlo A. Furia


// swap equal-length slices c[l..p) and c[p..h)
// (also guaranteeing inductiveness for rotation)
procedure swap_equal(a: [int]int, c: [int]int, low, high: int, l, h: int, p: int) returns(b: [int]int);
   requires low <= l && l < p && p < h && h <= high;
   requires p - l == h - p;
   // Out of bounds
   requires (forall i: int :: i < low ==> c[i] == a[i]);
   requires (forall i: int :: high <= i ==> c[i] == a[i]);
   // Already rotated on the left
   requires (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // Already rotated on the right
   requires (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // To be rotated
   requires (forall i: int :: l - low <= i && i < h - low 
            ==> 
            rot(a, low, high, p - low)[i] == rot(c, l, h, p - l)[i - (l - low)]);
   // Unchanged
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
   // Already rotated on the left: until p
   ensures (forall i: int :: 0 <= i && i < p - low
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // Already rotated on the right: from p
   ensures (forall i: int :: p - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);


// swap equal-length slices c[l..p) and c[h - (p - l)..h)
// (also guaranteeing inductiveness for rotation)
procedure swap_left(a: [int]int, c: [int]int, low, high: int, l, h: int, p: int) returns(b: [int]int);
   requires low <= l && l < p && p < h && h <= high;
   requires p - l < h - p;
   // Out of bounds
   requires (forall i: int :: i < low ==> c[i] == a[i]);
   requires (forall i: int :: high <= i ==> c[i] == a[i]);
   // Already rotated on the left
   requires (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // Already rotated on the right
   requires (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // To be rotated
   requires (forall i: int :: l - low <= i && i < h - low 
            ==> 
            rot(a, low, high, p - low)[i] == rot(c, l, h, p - l)[i - (l - low)]);
   // Unchanged
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
   // Already rotated on the left: as on input 
   ensures (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // Already rotated on the right: one more slice
   ensures (forall i: int :: h - low - (p - l) <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // To be rotated: smaller section
   ensures (forall i: int :: l - low <= i && i < h - low - (p - l) 
            ==> 
            rot(a, low, high, p - low)[i] == rot(b, l, h - (p - l), p - l)[i - (l - low)]);


// swap equal-length slices c[l..l + (h - p)) and c[h - p..h)
// (also guaranteeing inductiveness for rotation)
procedure swap_right(a: [int]int, c: [int]int, low, high: int, l, h: int, p: int) returns(b: [int]int);
   requires low <= l && l < p && p < h && h <= high;
   requires p - l > h - p;
   // Out of bounds
   requires (forall i: int :: i < low ==> c[i] == a[i]);
   requires (forall i: int :: high <= i ==> c[i] == a[i]);
   // Already rotated on the left
   requires (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // Already rotated on the right
   requires (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // To be rotated
   requires (forall i: int :: l - low <= i && i < h - low 
            ==> 
            rot(a, low, high, p - low)[i] == rot(c, l, h, p - l)[i - (l - low)]);
   // Unchanged
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
   // Already rotated on the left: one more slice
   ensures (forall i: int :: 0 <= i && i < l - low + (h - p) 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // Already rotated on the right: as on input 
   ensures (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // To be rotated: smaller section
   ensures (forall i: int :: l - low + (h - p) <= i && i < h - low  
            ==> 
            rot(a, low, high, p - low)[i] == 
            rot(b, l + (h - p), h, p - (l + (h - p)))[i - (l - low + h - p)]);



// Rotate a[low..high) to the left by r by swapping iteratively 
// Left-rotate a by r by swapping equal segments iteratively
procedure rotate_swap_iterative(a: [int]int, N: int, r: int) returns(b: [int]int)
   requires 0 <= r && r < N;
   ensures (forall i: int :: 0 <= i && i < N ==> seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
{

   var low, p, high: int;

   low, p, high := 0, r, N;

   b := a;

   if (r == 0) {
      return;
   }

   while (low < p && p < high)
   invariant 0 <= low && low <= p && p <= high && high <= N;
   invariant low == p <==> p == high;
   // Already rotated on the left
   invariant (forall i: int :: 0 <= i && i < low
             ==>
             rot(a, 0, N, p)[i] == seq(b, 0, N)[i]);
   // To be rotated
   invariant p - low < high - low ==> // antecedent added 2014-11-07
             (forall i: int :: low <= i && i < high
             ==> 
             rot(a, 0, N, p)[i] == rot(b, low, high, p - low)[i - low]);
   // Already rotated on the right
   invariant (forall i: int :: high <= i && i < N 
             ==>
             rot(a, 0, N, p)[i] == seq(b, 0, N)[i]);
   // Out of bounds
   invariant (forall i: int :: i < 0 ==> b[i] == a[i]);
   invariant (forall i: int :: N <= i ==> b[i] == a[i]);
   {
      goto equal_length, left_smaller, right_smaller;
      equal_length:
         assume p - low == high - p;
         call b := swap_equal(a, b, 0, N, low, high, p);
         low, high := low + (p - low), high - (high - p);
         goto continue;
      left_smaller:
         assume p - low < high - p;
         call b := swap_left(a, b, 0, N, low, high, p);
         high := high - (p - low);
         goto continue;
      right_smaller:
         assume p - low > high - p;
         call b := swap_right(a, b, 0, N, low, high, p);
         low := low + (high - p);
         goto continue;
      continue:
   }
   // assert low == p && p == high;
}
