// Copyright 2014 Carlo A. Furia


// i mod N
function wrap(i: int, N: int) returns(int);
axiom (forall i, N: int :: 0 <= i && i < N ==> wrap(i, N) == i);
axiom (forall i, N: int :: 0 < N && N <= i ==> wrap(i, N) == wrap(i - N, N));

procedure lemma_wrap_bounds(i: int, N: int)
   requires 0 < N;
   requires 0 <= i;
   ensures 0 <= wrap(i, N) && wrap(i, N) < N;
{
   // proof by induction
   if (i < N) {
   } else {
     call lemma_wrap_bounds(i - N, N);
   }
}


// Left-rotated sequence of a[low..high) at r
function rot(a: [int]int, low: int, high: int, r: int) returns([int]int);
axiom (forall a: [int]int, low: int, high: int, i, r: int :: 
      0 <= r && r < high - low && 0 <= i && i < r 
      ==> 
      rot(a, low, high, r)[i + high - low - r] == seq(a, low, high)[i]);
axiom (forall a: [int]int, low: int, high: int, i, r: int :: 
      0 <= r && r < high - low && r <= i && i < high - low 
      ==> 
      rot(a, low, high, r)[i - r] == seq(a, low, high)[i]);


// Equivalent representation of indexes   
procedure lemma_rot(a: [int]int, low, high: int, r: int, p: int)
   requires low <= high;
   requires 0 <= r && r < high - low;
   requires 0 <= p && p < high - low;
   ensures rot(a, low, high, r)[p] == seq(a, low, high)[wrap(r + p, high - low)];
{
}
