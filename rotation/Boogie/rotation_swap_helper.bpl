// Copyright 2014 Carlo A. Furia


//   a = X Y Z, with |X| = |Z| = d
// swapped into:
//   c = Z Y X
// rotated into:
//   b = Y Z X = rot(a, d)
procedure lemma_left_smaller(a: [int]int, al, ah: int, 
                             c: [int]int, cl, ch: int, 
                             b: [int]int, bl, bh: int,
                             d: int)
   requires al < ah;
   requires ah - al == bh - bl;
   requires ah - al == ch - cl;
   requires 0 < d && d < ah - al - d;
   requires (forall i: int :: ah - (d + al) <= i && i < ah - al 
            ==>
            rot(a, al, ah, d)[i] == seq(b, bl, bh)[i]);
   requires (forall i: int :: 0 <= i && i < ah - (d + al) 
            ==>
            rot(c, cl, ch - d, d)[i] == seq(b, bl, bh - d)[i]);
   requires (forall i: int :: 0 <= i && i < d 
            ==>
            seq(c, cl, ch)[i] == seq(a, al, ah)[i + (ah - d - al)]);
   requires (forall i: int :: d <= i && i < ah - (d + al) 
            ==>
            seq(c, cl, ch)[i] == seq(a, al, ah)[i]);
   ensures (forall i: int :: 0 <= i && i < ah - al 
           ==>
           rot(a, al, ah, d)[i] == seq(b, bl, bh)[i]);
{
   assert (forall i: int :: 0 <= i && i < ah - (d + al) 
          ==>
          rot(c, cl, ch - d, d)[i] == seq(b, bl, bh)[i]);
   assert (forall i: int :: 0 <= i && i < ch - cl - d - d 
          ==>
          rot(c, cl, ch - d, d)[i] == seq(c, cl, ch - d)[i + d]);
   assert (forall i: int :: 0 <= i && i < ch - cl - d - d 
          ==>
          rot(c, cl, ch - d, d)[i] == seq(c, cl, ch)[i + d]);
   assert (forall i: int :: ch - cl - d - d <= i && i < ch - cl - d 
          ==>
          rot(c, cl, ch - d, d)[i] == seq(c, cl, ch - d)[i - (ch - cl - d - d)]);
   assert (forall i: int :: ch - cl - d - d <= i && i < ch - cl - d 
          ==>
          rot(c, cl, ch - d, d)[i] == seq(c, cl, ch)[i - (ch - cl - d - d)]);
   assert (forall i: int :: 0 <= i && i < ah - (d + al) 
          ==>
          rot(a, al, ah, d)[i] == seq(b, bl, bh)[i]);
   assert (forall i: int :: ah - (d + al) <= i && i < ah - al 
          ==>
          rot(a, al, ah, d)[i] == seq(b, bl, bh)[i]);
}


//   a = X Y Z, with |X| = |Z| = d
// swapped into:
//   c = Z Y X
// rotated into:
//   b = Z X Y = rot(a, ah - al - d)
procedure lemma_right_smaller(a: [int]int, al, ah: int, 
                             c: [int]int, cl, ch: int, 
                             b: [int]int, bl, bh: int,
                             d: int)
   requires al < ah;
   requires ah - al == bh - bl;
   requires ah - al == ch - cl;
   requires 0 < d && d < ah - al - d;
   requires (forall i: int :: 0 <= i && i < d 
            ==>
            rot(a, al, ah, ah - al - d)[i] == seq(b, bl, bh)[i]);
   requires (forall i: int :: 0 <= i && i < ah - (d + al)  
            ==>
            rot(c, cl + d, ch, ah - al - d - d)[i] == seq(b, bl + d, bh)[i]);
   requires (forall i: int :: ah - (d + al) <= i && i < ah - al 
            ==>
            seq(c, cl, ch)[i] == seq(a, al, ah)[i - (ah - d - al)]);
   requires (forall i: int :: d <= i && i < ah - (d + al) 
            ==>
            seq(c, cl, ch)[i] == seq(a, al, ah)[i]);
   ensures (forall i: int :: 0 <= i && i < ah - al 
           ==>
           rot(a, al, ah, ah - al - d)[i] == seq(b, bl, bh)[i]);
{
   assert (forall i: int :: 0 <= i && i < ah - (d + al) 
          ==> 
          rot(c, cl + d, ch, ah - al - d - d)[i] == seq(b, bl, bh)[i + d]);
   assert (forall i: int :: d <= i && i < ch - cl - d 
          ==> 
          rot(c, cl + d, ch, ah - al - d - d)[i] == seq(c, cl + d, ch)[i - d]);
   assert (forall i: int :: 0 <= i && i < ch - cl - d - d 
          ==> 
          rot(c, cl + d, ch, ah - al - d - d)[i + d] == seq(c, cl + d, ch)[i]);
   assert (forall i: int :: d <= i && i < ch - cl - d 
          ==> 
          rot(c, cl + d, ch, ah - al - d - d)[i] == seq(c, cl, ch)[i]);
   assert (forall i: int :: 0 <= i && i < d  
          ==> 
          rot(c, cl + d, ch, ah - al - d - d)[i] == seq(c, cl + d, ch)[i + (ch - cl - d - d)]);
   assert (forall i: int :: 0 <= i && i < d 
          ==>
          rot(c, cl + d, ch, ah - al - d - d)[i] == seq(c, cl, ch)[i + (ch - cl - d)]);
   assert (forall i: int :: d <= i && i < ah - al 
          ==>
          rot(a, al, ah, ah - al - d)[i] == seq(b, bl, bh)[i]);
   assert (forall i: int :: { seq(b, bl, bh)[i] }  // Trigger needed to limit instantiation 
          0 <= i && i < d
          ==>
          rot(a, al, ah, ah - al - d)[i] == seq(b, bl, bh)[i]);
}



// swap a[low..low + d) and a[high - d..high)
// (An implementation is proved elsewhere)
procedure swap_sections(a: [int]int, low, high: int, d: int) returns(b: [int]int);
   requires 0 <= d && d <= high - low;
   requires low + d <= high - d;    // non overlapping sections
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: 0 <= i && i < d
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i + (high - low - d)]);
   ensures (forall i: int :: d <= i && i < high - low - d 
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i]);
   ensures (forall i: int :: high - low - d <= i && i < high - low
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i - (high - low - d)]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);


// Rotate a[low..p..high) at p by swapping recursively 
procedure rotate_swap_helper(a: [int]int, p: int, low, high: int) returns(b: [int]int)
   requires low <= p && p < high;
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: 0 <= i && i < high - low 
           ==>
           rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
{
   // GHOST
   var c: [int]int;  // value of b before recursive call

   if (p == low) {
      b := a;
      return;
   }

   if (p - low <= high - p) {
      // swap b[low..p) and b[high - (p - low)..high)
      call b := swap_sections(a, low, high, p - low);
      if (p - low == high - p) {
         return;
      } else {
         // now b[high - (p - low)..high) is in place

         // assert p - low < high - p;
         // assert (forall i: int :: high - p <= i && i < high - low
         //        ==>
         //        seq(b, low, high)[i] == seq(a, low, high)[i - (high - p)]);
         // assert (forall i: int :: high - p <= i && i < high - low 
         //        ==> 
         //        rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
         // assert (forall i: int :: 0 <= i && i < p - low 
         //        ==> 
         //        seq(b, low, high)[i] == seq(a, low, high)[i + (high - p)]);
         // assert (forall i: int :: p - low <= i && i < high - p 
         //        ==> 
         //        seq(b, low, high)[i] == seq(a, low, high)[i]);

         c := b;   // GHOST
         // assert (forall i: int :: 0 <= i && i < p - low 
         //        ==> 
         //        seq(c, low, high)[i] == seq(a, low, high)[i + (high - p)]);
         // assert (forall i: int :: p - low <= i && i < high - p 
         //        ==> 
         //        seq(c, low, high)[i] == seq(a, low, high)[i]);

         call b := rotate_swap_helper(b, p, low, high - (p - low));
         // assert (forall i: int :: high - p <= i && i < high - low 
         //        ==> 
         //        rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
         // assert (forall i: int :: 0 <= i && i < high - p 
         //        ==>
         //        rot(c, low, high - (p - low), p - low)[i] == seq(b, low, high - (p - low))[i]);
         // assert (forall i: int :: high - p <= i && i < high - low 
         //        ==> 
         //        rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
         // assert (forall i: int :: 0 <= i && i < p - low 
         //        ==> 
         //        seq(c, low, high)[i] == seq(a, low, high)[i + (high - p)]);
         // assert (forall i: int :: p - low <= i && i < high - p 
         //        ==> 
         //        seq(c, low, high)[i] == seq(a, low, high)[i]);

         call lemma_left_smaller(a, low, high, c, low, high, b, low, high, p - low);
      }
   } else { 
      // A little nudge to match swap_sections's precondition
      assert p - low > high - p;
      assert 0 <= high - p && high - p <= high - low;
      // swap b[p..high) and b[low..low + (high - p))
      call b := swap_sections(a, low, high, high - p);
      // now b[low..low + (high - p)) is in place

      c := b;   // GHOST

      call b := rotate_swap_helper(b, p, low + (high - p), high);

      call lemma_right_smaller(a, low, high, c, low, high, b, low, high, high - p);
   }
}
