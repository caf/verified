// Copyright 2014 Carlo A. Furia


// Sequence (0-indexed) of a[low..high)
function seq(a: [int]int, low: int, high: int) returns([int]int);
axiom (forall a: [int]int, low: int, high: int, i: int :: 
      0 <= i && i < high - low 
      ==> 
      seq(a, low, high)[i] == a[low + i]);
