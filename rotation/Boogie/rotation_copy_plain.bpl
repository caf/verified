// Copyright 2014 Carlo A. Furia


// Sequence (0-indexed) of a[low..high)
function seq(a: [int]int, low: int, high: int) returns([int]int);
axiom (forall a: [int]int, low: int, high: int, i: int :: 
      0 <= i && i < high - low 
      ==> 
      seq(a, low, high)[i] == a[low + i]);

// i mod N
function wrap(i: int, N: int) returns(int);
axiom (forall i, N: int :: 0 <= i && i < N ==> wrap(i, N) == i);
axiom (forall i, N: int :: 0 < N && N <= i ==> wrap(i, N) == wrap(i - N, N));

// Left-rotated sequence of a[low..high) at r  (directly defined using wrap)
function rot(a: [int]int, low: int, high: int, r: int) returns([int]int);
axiom (forall a: [int]int, low: int, high: int, i, r: int :: 
      0 <= r && r < high - low && 0 <= i && i < high - low 
      ==> 
      rot(a, low, high, r)[i] == seq(a, low, high)[wrap(i + r, high - low)]);


// Left-rotate a by r by copying
procedure rotate_copy(a: [int]int, N: int, r: int) returns(b: [int]int)
  requires 0 <= r && r < N;
  ensures (forall i: int :: 0 <= i && i < N ==> seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
{
   var s, d: int;
   if (r == 0) {  // In this case, rotation coincide with identity
      b := a;
      return;
   }
   s := 0;
   d := N - r;
   while (s < N)
   invariant (0 <= s && s <= N);
   invariant (d == wrap(s + N - r, N));
   invariant (0 <= d && d < N);
   invariant (forall i: int :: 
             0 <= i && i < s 
             ==> 
             seq(a, 0, N)[i] == seq(b, 0, N)[wrap(i + N - r, N)]);
   {
      b[d] := a[s];
      s := s + 1;
      d := d + 1;
      if (d == N) {
         d := 0;
      }
   }
}
