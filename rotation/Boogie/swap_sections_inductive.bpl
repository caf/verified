// Copyright 2014 Carlo A. Furia


// swap a[low..low + d) and a[high - d..high)
// (An implementation is proved elsewhere)
procedure swap_sections(a: [int]int, low, high: int, d: int) returns(b: [int]int);
   requires 0 <= d && d <= high - low;
   requires low + d <= high - d;    // non overlapping sections
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: 0 <= i && i < d
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i + (high - low - d)]);
   ensures (forall i: int :: d <= i && i < high - low - d 
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i]);
   ensures (forall i: int :: high - low - d <= i && i < high - low
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i - (high - low - d)]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);


procedure swap_equal(a: [int]int, c: [int]int, low, high: int, l, h: int, p: int) returns(b: [int]int)
   requires low <= l && l < p && p < h && h <= high;
   // left same size as right
   requires p - l == h - p;
   // Out of bounds
   requires (forall i: int :: i < low ==> c[i] == a[i]);
   requires (forall i: int :: high <= i ==> c[i] == a[i]);
   // Already rotated on the left
   requires (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // Already rotated on the right
   requires (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // To be rotated
   requires (forall i: int :: l - low <= i && i < h - low 
            ==> 
            rot(a, low, high, p - low)[i] == rot(c, l, h, p - l)[i - (l - low)]);
   // Unchanged
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
   // Already rotated on the left: until p
   ensures (forall i: int :: 0 <= i && i < p - low
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // Already rotated on the right: from p
   ensures (forall i: int :: p - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
{
   call b := swap_sections(c, l, h, p - l);
      // assert added 2014-11-07
   assert (forall i: int :: 0 <= i && i < p - l
          ==>
          rot(c, l, h, p - l)[i] == seq(b, l, h)[i]);
      // assert added 2014-11-07
   assert (forall i: int :: p - l <= i && i < h - l
          ==>
          rot(c, l, h, p - l)[i] == seq(b, l, h)[i]);
}


// swap equal-length slices c[l..p) and c[h - (p - l)..h)
// (also guaranteeing inductiveness for rotation)
procedure swap_left(a: [int]int, c: [int]int, low, high: int, l, h: int, p: int) returns(b: [int]int)
   requires low <= l && l < p && p < h && h <= high;
   // left is smaller
   requires p - l < h - p;
   // Out of bounds
   requires (forall i: int :: i < low ==> c[i] == a[i]);
   requires (forall i: int :: high <= i ==> c[i] == a[i]);
   // Already rotated on the left
   requires (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // Already rotated on the right
   requires (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // To be rotated
   requires (forall i: int :: l - low <= i && i < h - low 
            ==> 
            rot(a, low, high, p - low)[i] == rot(c, l, h, p - l)[i - (l - low)]);
   // Unchanged
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
   // Already rotated on the left: as on input 
   ensures (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // Already rotated on the right: one more slice
   ensures (forall i: int :: h - low - (p - l) <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // To be rotated: smaller section
   ensures (forall i: int :: l - low <= i && i < h - low - (p - l) 
            ==> 
            rot(a, low, high, p - low)[i] == rot(b, l, h - (p - l), p - l)[i - (l - low)]);
{
   call b := swap_sections(c, l, h, p - l);
   // Part of swap_sections' postcondition
   assert (forall i: int :: { seq(b, l, h)[i] }
          0 <= i && i < p - l  
          ==>
          seq(b, l, h)[i] == seq(c, l, h)[i + (h - p)]);
   // [rot(b)]: first axiom defining rot
   assert (forall i: int :: 
          0 <= i && i < p - l 
          ==>
          rot(b, l, h - (p - l), p - l)[i + (h - p - p + l)] == seq(b, l, h - (p - l))[i]);
   // precondition 'to be rotated' with scaled indexes
   assert (forall i: int :: 
          0 <= i && i < h - l 
          ==>
          rot(a, low, high, p - low)[i + (l - low)] == rot(c, l, h, p - l)[i]);

   // [rot(c) right]: second axiom defining rot
   assert (forall i: int :: { seq(c, l , h)[i] }
          p - l <= i && i < h - p  
          ==>
          rot(c, l, h, p - l)[i - (p - l)] == seq(c, l, h)[i]);
   // part of swap_sections' postcondition
   assert (forall i: int :: 
          p - l <= i && i < h - p 
          ==>
          seq(b, l, h - (p - l))[i] == seq(c, l, h)[i]);
   // combining the previous two, matching seq(c, l, h)[i] as pivot 
   assert (forall i: int :: { seq(c, l , h)[i] }
          p - l <= i && i < h - p  
          ==>
          rot(b, l, h - (p - l), p - l)[i - (p - l)] == seq(c, l, h)[i]);
   // previous one with scaled indexes
   assert (forall i: int :: { rot(b, l, h - (p - l), p - l)[i] }
          0 <= i && i < h - p - p + l
          ==>
          rot(b, l, h - (p - l), p - l)[i] == seq(c, l, h)[i + (p - l)]);
   // [left]: previous one and definition of rot(c, l, h, p - l) recalled in [rot(c) right] above
   assert (forall i: int ::
          0 <= i && i < h - p - p + l
          ==>
          rot(b, l, h - (p - l), p - l)[i] == rot(c, l, h, p - l)[i]);

   // [rot(c) left]: first axiom defining rot
   assert (forall i: int :: { seq(c, l , h)[i] }
          0 <= i && i < p - l  
          ==>
          rot(c, l, h, p - l)[i + (h - p)] == seq(c, l, h)[i]);
   // part of swap_sections' postcondition
   assert (forall i: int :: 
          0 <= i && i < p - l  
          ==>
          seq(b, l, h - (p - l))[i] == seq(c, l, h)[i + (h - p)]);
   // previous one and definition of rot(b) recalled in [rot(b)] above
   assert (forall i: int :: 
          0 <= i && i < p - l  
          ==>
          rot(b, l, h - (p - l), p - l)[i + (h - p - p + l)] == seq(c, l, h)[i + (h - p)]);
   // previous one with scaled indexes 
   assert (forall i: int :: 
          h - p - p + l <= i && i < h - p
          ==>
          rot(b, l, h - (p - l), p - l)[i] == seq(c, l, h)[i + (p - l)]);
   // [right]: previous one and definition of rot(c, l, h, p - l) recalled in [rot(c) left] above
   assert (forall i: int ::
          h - p - p + l <= i && i < h - p 
          ==>
          rot(b, l, h - (p - l), p - l)[i] == rot(c, l, h, p - l)[i]);

   // combine [left] and [right] to sweep the whole range in post 'smaller section'
} 



// swap equal-length slices c[l..l + (h - p)) and c[h - p..h)
// (also guaranteeing inductiveness for rotation)
// (two assumes per branch that seem obvious cannot be proved as assert)
procedure swap_right(a: [int]int, c: [int]int, low, high: int, l, h: int, p: int) returns(b: [int]int)
   requires low <= l && l < p && p < h && h <= high;
   requires p - l > h - p;
   // Out of bounds
   requires (forall i: int :: i < low ==> c[i] == a[i]);
   requires (forall i: int :: high <= i ==> c[i] == a[i]);
   // Already rotated on the left
   requires (forall i: int :: 0 <= i && i < l - low
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // Already rotated on the right
   requires (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(c, low, high)[i]);
   // To be rotated
   requires (forall i: int :: l - low <= i && i < h - low 
            ==> 
            rot(a, low, high, p - low)[i] == rot(c, l, h, p - l)[i - (l - low)]);
   // Unchanged
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
   // Already rotated on the left: one more slice
   ensures (forall i: int :: 0 <= i && i < l - low + (h - p) 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // Already rotated on the right: as on input 
   ensures (forall i: int :: h - low <= i && i < high - low 
            ==>
            rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);
   // To be rotated: smaller section
   ensures (forall i: int :: l - low + (h - p) <= i && i < h - low  
            ==> 
            rot(a, low, high, p - low)[i] == 
            rot(b, l + (h - p), h, p - (l + (h - p)))[i - (l - low + h - p)]);
{
   call b := swap_sections(c, l, h, h - p);
   // Extra slice
   assert (forall i: int :: 
          0 <= i && i < h - p 
          ==> 
          seq(b, l, h)[i] == seq(c, l, h)[i + (p - l)]);
   assert (forall i: int :: 
          l - low <= i && i < l - low + (h - p) 
          ==>
          rot(a, low, high, p - low)[i] == seq(b, low, high)[i]);

   // [rot(c) left]: first axiom defining rot
   assert (forall i: int :: { seq(c, l, h)[i] }
          0 <= i && i < p - l  
          ==> 
          rot(c, l, h, p - l)[i + (h - p)] == seq(c, l, h)[i]);
   // [rot(c) left bis]: special case of previous one (subinterval)
   assert (forall i: int :: { seq(c, l, h)[i] }
          0 <= i && i < h - p   
          ==> 
          rot(c, l, h, p - l)[i + (h - p)] == seq(c, l, h)[i]);
   // [rot(c) left ter]: special case of [rot(c) left]
   assert (forall i: int :: { seq(c, l, h)[i] }
          h - p <= i && i < p - l 
          ==> 
          rot(c, l, h, p - l)[i + (h - p)] == seq(c, l, h)[i]);

   // [rot(b)]: second axiom defining rot
   assert (forall i: int :: 
          p - l - h + p <= i && i < p - l  
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i - (p - l - h + p)] == seq(b, l + (h - p), h)[i]);
   // [rot(c) right]: second axiom defining rot
   assert (forall i: int :: 
          p - l <= i && i < h - l 
          ==>
          rot(c, l, h, p - l)[i - (p - l)] == seq(c, l, h)[i]);
   // part of swap_sections' postcondition
   assert (forall i: int ::
          p - l - h + p <= i && i < p - l  
          ==>
          seq(b, l + (h - p), h)[i] == seq(c, l, h)[i - (p - l - h + p)]);
   // previous one and definition of rot(b) recalled in [rot(b)] above
   assume (forall i: int :: 
          p - l - h + p <= i && i < p - l 
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i - (p - l - h + p)] 
             == seq(c, l, h)[i - (p - l - h + p)]);
   // previous one with scaled indexes 
   assume (forall i: int :: 
          0 <= i && i < h - p 
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i] == seq(c, l, h)[i]);
   // [left]: previous one and definition of rot(c, l, h, p - l) recalled in [rot(c) left bis] above
   assert (forall i: int ::
          0 <= i && i < h - p 
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i] == rot(c, l, h, p - l)[i + (h - p)]);


   // [rot(b)]: first axiom defining rot
   assert (forall i: int :: { seq(b, l + (h - p), h)[i] }
          0 <= i && i < p - l - h + p 
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i + (h - p)] == seq(b, l + (h - p), h)[i]);
   // part of swap_sections' postcondition (with scaled indexes)
   assert (forall i: int :: { seq(c, l, h)[i + (h - p)] }
          0 <= i && i < p - l - h + p 
          ==>
          seq(b, l + (h - p), h)[i] == seq(c, l, h)[i + (h - p)]);
   // previous one and definition of rot(b) (first axiom)
   assume (forall i: int :: 
          0 <= i && i < p - l - h + p 
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i + (h - p)] == seq(c, l, h)[i + (h - p)]);  
   // previous one with scaled indexes
   assume (forall i: int :: 
          h - p <= i && i < p - l 
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i] == seq(c, l, h)[i]); 
   // [right]: previous one and definition of rot(c, l, h, p - l) recalled in [rot(c) left] above
   assert (forall i: int ::
          h - p <= i && i < p - l 
          ==>
          rot(b, l + (h - p), h, p - l - h + p)[i] == rot(c, l, h, p - l)[i + (h - p)]);

   // combine [left] and [right] to sweep the whole range in post 'smaller section'
}
