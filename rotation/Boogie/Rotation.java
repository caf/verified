// Copyright 2014 Carlo A. Furia


import java.util.Arrays;
import java.io.*;

public class Rotation {

	 public static final int DEFAULT_SIZE = 1500;
	 public static final int HUGE_SIZE = Integer.MAX_VALUE - 8;

	 public static final String ALGO_COPY = "copy";
	 public static final String ALGO_COPY_NATIVE = "copy_native";
	 public static final String ALGO_REVERSE = "reverse";
	 public static final String ALGO_SWAP = "swap";
	 public static final String ALGO_MODULO = "modulo";

	 public static void print_usage() {
		  System.out.println("Usage: java Rotation algo [size] [--test] [--printall]");
		  System.out.println("  size:  maximum size of test arrays (default: " + DEFAULT_SIZE + ", -1 for huge size)");
		  System.out.println("  algo:  algorithm used for rotation (one of " +
									"\"" + ALGO_COPY + "\", " +
									"\"" + ALGO_COPY_NATIVE + "\", " +
									"\"" + ALGO_REVERSE + "\", " +
									"\"" + ALGO_SWAP + "\", " +
									"\"" + ALGO_MODULO + "\")");
		  System.out.println("  --test:  test correctness of output (default: no)");
		  System.out.println("  --printall:  log on file time for each call (default: no)");
	 }

	 public static void main(String[] args) {
		  if (args.length < 1 || args.length > 4) {
				print_usage();
				return;
		  }

		  String algo = args[0];
		  if (!algo.equals(ALGO_COPY) &&
				!algo.equals(ALGO_COPY_NATIVE) &&
				!algo.equals(ALGO_REVERSE) &&
				!algo.equals(ALGO_SWAP) &&
				!algo.equals(ALGO_MODULO)) {
				System.out.println("Unrecognized algorithm: " + algo + "\n");
				print_usage();
				return;
		  }

		  int size = DEFAULT_SIZE;
		  boolean do_test = false;
		  boolean print_all = false;

		  for (int i = 1; args.length > i; i++) {
				if (args[i].equals("--test")) {
					 do_test = true;
					 continue;
				}
				if (args[i].equals("--printall")) {
					 print_all = true;
					 continue;
				}
				try {
					 size = Integer.parseInt(args[i]);
				} catch (Exception e) {
					 size = 0;
				}
				if (size == 0 || size < -1) {
					 System.out.println("Invalid size: " + args[i] + "\n");
					 print_usage();
					 return;
				}
		  }

		  if (size == -1)
				size = HUGE_SIZE;


		  int[] test;
		  long total_time = 0;
		  long current_time;
		  long start, end;
		  int init_size = size;

		  // times[N][r] is the time to compute the r-rotation of an (N + 1)-element array
		  long[][] times = null;

		  BufferedWriter outlog = null;

		  if (print_all) {
				try {
					 outlog = new BufferedWriter(new FileWriter(new File(algo + "_" + size + ".table")));
					 outlog.write("ALGORITHM" + "\t" + "Array size" + "\t" + 
									  "Rotation by" + "\t" + "Time (ms)" + "\n");
				} catch (Exception e) {
					 System.out.println("Couldn't open output file. Option --printall ignored.");
					 print_all = false;
				}
		  }

		  if (do_test) {
				init_size = 1;
		  }

		  // For each array size
		  for (int t = init_size; t <= size; t++) {
				test = new int[t];
				// Initialize array
				for (int i = 0; i < t; i++)
					 test[i] = 100 + i;
				int[] tc = null;
				// For each rotation displacement
				for (int r = 0; r < t; r++) {
					 int[] b = null;
					 if (do_test) {
						  // Oracle result
						  b = rotation(test, r);
						  // test will be modified, tc is a backup
						  tc = Arrays.copyOf(test, test.length);
					 }
					 start = System.currentTimeMillis();
					 switch (algo) {
					 case ALGO_COPY:
						  rotate_copy(test, r);
						  break;
					 case ALGO_COPY_NATIVE:
						  rotate_copy_native(test, r);
						  break;
					 case ALGO_REVERSE:
						  rotate_reverse(test, r);
						  break;
					 case ALGO_SWAP:
						  rotate_swap(test, r);
						  break;
					 case ALGO_MODULO:
						  rotate_modulo(test, r);
						  break;
					 }
					 end = System.currentTimeMillis();
					 current_time = end - start;
					 total_time += end - start;
					 if (do_test) {
						  check(tc, test, b, r, algo);
					 }
					 if (print_all) {
						  String res = algo + "\t" + t + "\t" + r + "\t" + current_time  + "\n";
						  try {
								outlog.write(res);
						  } catch (Exception e) {
								System.out.println("Error writing result for: N = " + t + ",  = " + r);
						  }
					 }
				}
		  }

		  if (print_all) {
				try {
					 if (outlog != null)
						  outlog.close();
				} catch (Exception e) {
					 System.out.println("Error closing output file.");
				}
		  } else {
				System.out.println("Algorithm " + algo + " took " + total_time + " milliseconds overall.");
		  }
	 }


	 // Definition or reference implementation
	 public static int[] rotation(int[] a, int r) {
		  int N = a.length;
		  int[] result = new int[N];
		  for (int i = 0; i < N; i++) {
				result[(i + N - r) % N] = a[i];
		  }
		  return result;
	 }

	 private static void check(int[] test, int[] tc, int[] b, int r, String m) {
		  if (!Arrays.equals(tc, b)) {
				System.out.println("Error: rotation by " + r + " of " + Arrays.toString(test) + " using " + m);
				System.out.println("       returned " + Arrays.toString(tc) + " instead of " + Arrays.toString(b));
		  }
	 }



	 // Rotate `a' by `r' by copying
	 public static void rotate_copy(int[] a, int r) {
		  int N = a.length;
		  assert 0 <= r && r < N;

		  if (r == 0)
				return;

		  int[] b = new int[N];
		  for (int s = 0, d = N - r; s < N; ) {
				b[d] = a[s];
				s++;
				d++;
				if (d == N) {
					 d = 0;
				}
		  }
		  System.arraycopy(b, 0, a, 0, N);
	 }


	 // Rotate `a' by `r' by copying (using native methods)
	 public static void rotate_copy_native(int[] a, int r) {
		  int N = a.length;
		  assert 0 <= r && r < N;

		  if (r == 0)
				return;

		  int small = (r <= N - r) ? r : (N - r);

		  int[] b = new int[small];

		  if (r <= N - r) {
				System.arraycopy(a, 0, b, 0, r);
				System.arraycopy(a, r, a, 0, N - r);
				System.arraycopy(b, 0, a, N - r, r);
		  } else {
				System.arraycopy(a, r, b, 0, N - r);
				System.arraycopy(a, 0, a, N - r, r);
				System.arraycopy(b, 0, a, 0, N - r);
		  }
	 }


	 // Swap `a[low..p)' and `a[p..high)'
	 private static void rotate_swap_helper(int[] a, int p, int low, int high) {
		  assert 0 <= low && low <= p && p <= high && high <= a.length;

		  int t;

		  while (low < p && p < high) {
				if (p - low <= high - p) {
					 // swap a[low..p) and a[high - (p - low)..high)
					 for (int s = low, d = high - (p - low); s < p; s++, d++) {
						  t = a[d];
						  a[d] = a[s];
						  a[s] = t;
					 }
					 // now a[high - (p - low)..high) is in place
					 high = high - (p - low);
				} else {
					 // swap a[p..high) and a[low..low + (high - p))
					 for (int s = p, d = low; s < high; s++, d++) {
						  t = a[d];
						  a[d] = a[s];
						  a[s] = t;
					 }
					 // now a[low..low + (high - p)) is in place
					 low = low + (high - p);
				}
		  }
	 }


	 public static void rotate_swap(int[] a, int r) {
		  int N = a.length;
		  assert 0 <= r && r < N;

		  rotate_swap_helper(a, r, 0, N);
	 }


	 // Rotate `a' by `r' by performing 3 reversals
	 public static void rotate_reverse(int[] a, int r) {
		  int N = a.length;
		  assert 0 <= r && r < N;

		  reverse(a, 0, r);
		  reverse(a, r, N);
		  reverse(a, 0, N);
	 }


	 // Rotate `a' by `r' by modular copying
	 public static void rotate_modulo(int[] a, int r) {
		  int N = a.length;
		  assert 0 <= r && r < N;

		  for (int cycleStart = 0, nMoved = 0; nMoved != N; cycleStart++) {
				int displaced = a[cycleStart], t;
				int i = cycleStart;
				do {
					 i = i + N - r;
					 if (i >= N)
						  i = i - N;
					 t = a[i];
					 a[i] = displaced;
					 displaced = t;
					 nMoved++;
				} while (i != cycleStart);
		  }
	 }


	 // Reverse `a'[`low'..`high')
	 public static void reverse(int[] a, int low, int high) {
		  int i, j, t;
		  i = low;
		  j = high - 1;
		  while (i < j) {
				t = a[i];
				a[i] = a[j];
				a[j] = t;
				i++;
				j--;
		  }
	 }

}
