// Copyright 2014 Carlo A. Furia


// p-th modular position: over N-element array, modulo m, starting at s
function mp(N: int, m: int, s: int, p: int) returns(q: int);
axiom (forall N: int, m: int, s: int :: 0 < m && m < N && 0 <= s && s < N ==> mp(N, m, s, 0) == s);
axiom (forall N: int, m: int, s: int, p: int :: 0 < m && m < N && 0 <= s && s < N && 0 < p 
      ==> mp(N, m, s, p) == wrap(mp(N, m, s, p - 1) + m, N));


procedure lemma_mp(N: int, m: int, s: int, p: int)
   requires 0 < m && m < N;
   requires 0 <= s && s < N;
   requires 0 <= p;
   ensures 0 <= mp(N, m, s, p) && mp(N, m, s, p) < N;
{ 
  // proof by induction
  if (p == 0) {
  } else {
    call lemma_mp(N, m, s, p - 1);
  }
}


procedure lemma_rotmp(s: int, a: [int]int, low: int, high: int, r: int, k: int)
   requires 0 < r && r < high - low;
   requires 0 <= s && s < high - low;
   requires 0 < k;
   ensures rot(a, low, high, r)[mp(high - low, high - low - r, s, k)]
           ==
           seq(a, low, high)[mp(high - low, high - low - r, s, k - 1)];
{
  if (k == 1) {
  } else {
    // lemma_mp makes it possible to apply the definition of rot
    call lemma_mp(high - low, high - low - r, s, k - 1);
  }
}


// ic(N, m) == N div gcd(N, m), but I avoid axiomatizing div
// ic(N, m) denotes the period of a cycle of step gcd(N, m) over N elements
function ic(N: int, m: int) returns(x: int);
axiom (forall N: int, m: int :: 0 <= m && m < N ==> 0 < ic(N, m) && ic(N, m) <= N);
axiom (forall N: int, m: int, s: int :: mp(N, m, s, ic(N, m)) == s);
axiom (forall N: int, m: int, s: int, p: int :: 0 < p && p < ic(N, m) ==> mp(N, m, s, p) != s);
// These would be lemmas following from complete definitions of div and gcd
axiom (forall N: int, m: int, s: int, p, q: int :: 
       0 <= p && p < ic(N, m) && 0 <= q && q < ic(N, m) && p != q 
       ==> 
       mp(N, m, s, p) != mp(N, m, s, q));
axiom (forall N: int, m: int, s, t: int, p, q: int :: 
       0 <= p && p < ic(N, m) && 0 <= q && q < ic(N, m) && 0 <= s && s < t && t < s + gcd(N, m) && t < N
       ==> 
       mp(N, m, s, p) != mp(N, m, t, q));


// Basic facts about gcd
function gcd(x: int, y: int) returns(z: int);
axiom (forall x, y: int :: 0 < x && 0 < y ==> 0 < gcd(x, y) && gcd(x, y) <= x && gcd(x, y) <= y);
// This is because ic(N, m) == N div gcd(N, m)
axiom (forall N: int, m: int :: 0 < m && m < N ==> gcd(N, m) * ic(N, m) == N);


// yp is like an inverse of mp:
// yp(N, m, s, i) == p such that mp(N, m, s, p) == i
function yp(N: int, m: int, s: int, i: int) returns(y: int);
axiom (forall N: int, m: int, i: int :: 
      0 < m && m < N && 0 <= i && i < N
      ==>
      0 <= yp(N, m, wrap(i, gcd(N, m)), i) && yp(N, m, wrap(i, gcd(N, m)), i) < ic(N, m));
axiom (forall N: int, m: int, i: int :: 
      0 < m && m < N && 0 <= i && i < N
      ==>
      mp(N, m, wrap(i, gcd(N, m)), yp(N, m, wrap(i, gcd(N, m)), i)) == i);


procedure lemma_yp_mp(N: int, m: int, i: int, set: [int]bool)
   requires 0 <= i && i < N;
   requires 0 < m && m < N;
   requires 0 <= wrap(i, gcd(N, m)) && wrap(i, gcd(N, m)) < gcd(N, m);
   requires 0 <= yp(N, m, wrap(i, gcd(N, m)), i) && yp(N, m, wrap(i, gcd(N, m)), i) < ic(N, m);
   requires mp(N, m, wrap(i, gcd(N, m)), yp(N, m, wrap(i, gcd(N, m)), i)) == i;
   requires set[mp(N, m, wrap(i, gcd(N, m)), yp(N, m, wrap(i, gcd(N, m)), i))];
   ensures set[i];
{
}


// This should follow for a forall applied to lemma_yp_mp, 
// but quantifier instantiation doesn't work as expected. 
// Hence the free ensures.
procedure lemma_extensional(N: int, m: int, set: [int]bool)
   requires 0 < m && m < N;
   requires (forall i: int ::
            0 <= i && i < N 
            ==>
            0 <= wrap(i, gcd(N, m)) && wrap(i, gcd(N, m)) < gcd(N, m));
   requires (forall i: int ::
            0 <= i && i < N 
            ==>
            0 <= yp(N, m, wrap(i, gcd(N, m)), i) && yp(N, m, wrap(i, gcd(N, m)), i) < ic(N, m));
   requires (forall i: int ::
            0 <= i && i < N 
            ==>
            mp(N, m, wrap(i, gcd(N, m)), yp(N, m, wrap(i, gcd(N, m)), i)) == i);
   requires (forall i: int ::
            0 <= i && i < N 
            ==>
            set[mp(N, m, wrap(i, gcd(N, m)), yp(N, m, wrap(i, gcd(N, m)), i))]);
   free ensures (forall i: int :: 0 <= i && i < N ==> set[i]);
{
   call forall lemma_yp_mp(N, m, *, set);
}
