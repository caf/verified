// Copyright 2014 Carlo A. Furia



// Left-rotate a by r by modular visit of its elements
// Key correctness argument: each rotation is a permutation that can be decomposed into 
//                           gcd(N, N - r) cycles each of length N / gcd(N, N - r)
procedure rotate_modulo(a: [int]int, N: int, r: int) returns(b: [int]int)
  requires 0 <= r && r < N;
  ensures (forall i: int :: 0 <= i && i < N ==> seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
{
   var start: int; // start position of modular cycle
   var v: int; // current position in modular cycle
   var displaced: int; // currently displaced element

   // GHOST
   var k: int;  // k-th modular position in current cycle
   var set: [int]bool; // positions in b changed by loops

   b := a;
   // Initially, no positions set in b
   assume (forall i: int :: !set[i]);  // GHOST

   if (r == 0) {
      return;
   }

   start := 0;

   // The outer cycle iterates gcd(N, N - r) times
   while (start < gcd(N, N - r))  
   invariant (0 <= start && start <= gcd(N, N - r));
   invariant (forall i: int :: 0 <= i && i < N && !set[i] ==> b[i] == a[i]);
   invariant (forall i: int, s: int ::
             0 <= i && i < ic(N, N - r) && start <= s && s < gcd(N, N - r)
             ==> 
             !set[mp(N, N - r, s, i)]);
   invariant (forall i: int, s: int :: 
             0 <= i && i < ic(N, N - r) && 0 <= s && s < start
             ==> 
             set[mp(N, N - r, s, i)]);
   invariant (forall i: int :: 
             0 <= i && i < N && set[i]
             ==> 
             seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
   {
      displaced := b[start];
      v := start;
      k := 0; // GHOST

      // One iteration of the inner loop unconditionally executed
      k := k + 1; // GHOST
      v := v + N - r;
      if (v >= N) {
         v := v - N;
      }
      b[v], displaced := displaced, b[v];
      set[v] := true;  // GHOST

      call lemma_rotmp(start, a, 0, N, r, k);

      // Each inner cycle visits N / gcd(N, N - r) == ic(N, N - r) elements
      while (v != start) 
      invariant 0 <= v && v < N;
      invariant v == mp(N, N - r, start, k);
      invariant displaced == a[mp(N, N - r, start, k)];
      invariant (forall i: int :: 0 <= i && i < N && !set[i] ==> b[i] == a[i]);
      invariant 0 < k && k <= ic(N, N - r);
      invariant (forall i: int :: k < i && i <= ic(N, N - r) ==> !set[mp(N, N - r, start, i)]);
      invariant (forall i: int :: 0 < i && i <= k ==> set[mp(N, N - r, start, i)]);
      invariant (forall i: int, s: int :: 
                0 <= i && i < ic(N, N - r) && start < s && s < gcd(N, N - r)
                ==> 
                !set[mp(N, N - r, s, i)]);
      invariant (forall i: int, s: int :: 
                0 <= i && i < ic(N, N - r) && 0 <= s && s < start
                ==> 
                set[mp(N, N - r, s, i)]);
      invariant (forall i: int :: 
                0 <= i && i < N && set[i]
                ==> 
                seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
      {
         k := k + 1; // GHOST
         v := v + N - r;
         if (v >= N) {
            v := v - N;
         }
         b[v], displaced := displaced, b[v];
         set[v] := true;  // GHOST
         call forall lemma_mp(N, N - r, start, *);
         call lemma_rotmp(start, a, 0, N, r, k);
      }
      assert k == ic(N, N - r);
      start := start + 1;
   }
   assert (forall i: int, s: int :: 
          0 <= i && i < ic(N, N - r) && 0 <= s && s < gcd(N, N - r)
          ==> 
          set[mp(N, N - r, s, i)]);
   assert (forall i: int :: 
          0 <= i && i < N && set[i]
          ==> 
          seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
   assert 0 < N - r && N - r < N;
   call forall lemma_wrap_bounds(*, gcd(N, N - r));
   assert (forall i: int :: 0 <= i && i < N 
           ==> 
           set[mp(N, N - r, wrap(i, gcd(N, N - r)), yp(N, N - r, wrap(i, gcd(N, N - r)), i))]);
   call forall lemma_yp_mp(N, N - r, *, set);
   call lemma_extensional(N, N - r, set);
   assert (forall i: int :: 0 <= i && i < N ==> set[i]);
}
