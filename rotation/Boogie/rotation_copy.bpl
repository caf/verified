// Copyright 2014 Carlo A. Furia


// Left-rotate a by r by copying
procedure rotate_copy(a: [int]int, N: int, r: int) returns(b: [int]int)
  requires 0 <= r && r < N;
  ensures (forall i: int :: 0 <= i && i < N ==> seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
{
   var s, d: int;
   if (r == 0) {  // In this case, rotation coincide with identity
      b := a;
      return;
   }
   s, d := 0, N - r;
   while (s < N)
   invariant (0 <= s && s <= N);
   invariant (d == wrap(s + N - r, N));
   // invariant (0 <= d && d < N);
   invariant (forall i: int :: 0 <= i && i < s ==> seq(a, 0, N)[i] == seq(b, 0, N)[wrap(i + N - r, N)]);
   {
      b[d] := a[s];
      s, d := s + 1, d + 1;
      if (d == N) {
         d := 0;
      }
   }

   // Converting between wrap and axiomatic representation of rot:
   // Option 1: use a lemma
   // call forall lemma_rot(a, 0, N, r, *);
   // Option 2: express the lemma directly as assertion:
   assert (forall i: int :: 0 <= i && i < N ==> rot(a, 0, N, r)[i] == seq(a, 0, N)[wrap(i + r, N)]);
}
