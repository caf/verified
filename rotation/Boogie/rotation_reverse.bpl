// Copyright 2014 Carlo A. Furia


// Left-rotate a by r by performing three reversals.
// Key correctness argument: if |X| = r and |Y| = N - r, 
//                           then rot(X Y, r) = rev(rev(X) rev(Y)) = Y X
procedure rotate_reverse(a: [int]int, N: int, r: int) returns(b: [int]int)
  requires 0 <= r && r < N;
  ensures (forall i: int :: 0 <= i && i < N ==> seq(b, 0, N)[i] == rot(a, 0, N, r)[i]);
{
   b := a;
   call b := reverse(b, 0, r);
   call b := reverse(b, r, N);
   assert (forall i: int :: 0 <= i && i < r ==> seq(b, 0, r)[i] == rev(a, 0, r)[i]);
   assert (forall i: int :: 0 <= i && i < N - r ==> seq(b, r, N)[i] == rev(a, r, N)[i]);
   call forall lemma_rev_cat_2(a, 0, r, a, r, N, b, *);
   assert (forall i: int :: 0 <= i && i < r ==> rev(b, 0, r)[i] == seq(a, 0, r)[i]);
   assert (forall i: int :: 0 <= i && i < N - r ==> rev(b, r, N)[i] == seq(a, r, N)[i]);
   call b := reverse(b, 0, N);
   assert (forall i: int :: 0 <= i && i < N - r ==> seq(b, 0, N)[i] == seq(a, r, N)[i]);
   assert (forall i: int :: 0 <= i && i < r ==> seq(b, 0, N)[i + N - r] == seq(a, 0, r)[i]);
   call forall lemma_rot(a, 0, N, r, *);
   assert (forall i: int :: 0 <= i && i < N - r ==> rot(a, 0, N, r)[i] == seq(b, 0, N)[i]);
}
