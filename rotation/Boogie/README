This directory contains Boogie files of annotated implementations of
various algorithms to rotate arrays (represented as maps with bounds
in Boogie). I describe in detail the algorithms and their correctness
proofs in the following technical report:

    http://arxiv.org/abs/1406.5453

The directory also contains a Makefile to run Boogie on the annotated
inputs, and a simple Java implementation of the same algorithms proved
in Boogie. 

These files were developed with older versions of Boogie and Z3. In
the latest versions (Boogie >= 2.2.3, Z3 >= 4.3.2) some examples don't
work anymore: Boogie discontinued support for "call forall", and Z3
fails on some examples where it succeeded before (this might be due to
different VCs being generated, but I didn't look into it in
detail). With some work, it should be possible to adjust the examples
so that they work with the newest versions of Boogie and Z3.

All files in this directory are authored by Carlo A. Furia
(http://bugcounting.net).

The files in this directory are free software: you can redistribute
them and/or modify them under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version. A copy of the
GNU General Public License is available in the file COPYING in this
directory.

The files in this directory are distributed in the hope that they will
be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.
