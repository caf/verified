// Copyright 2014 Carlo A. Furia


// swap a[low..low + d) and a[high - d..high)
// (Used in rotate_swap)
procedure swap_sections(a: [int]int, low, high: int, d: int) returns(b: [int]int)
   requires low <= low + d && low + d <= high - d && high - d <= high;    // non overlapping slices
   ensures (forall i: int :: i < low ==> b[i] == a[i]);
   ensures (forall i: int :: 0 <= i && i < d
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i + (high - low - d)]);
   ensures (forall i: int :: d <= i && i < high - low - d 
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i]);
   ensures (forall i: int :: high - low - d <= i && i < high - low
           ==> 
           seq(b, low, high)[i] == seq(a, low, high)[i - (high - low - d)]);
   ensures (forall i: int :: high <= i ==> b[i] == a[i]);
{
   var x, z: int;  // Pointers to two slices
   var tmp: int;   // Temporary variable for swap

   b := a;
   x, z := low, high - d;

   while (x < low + d)
   invariant x - low == z - (high - d);
   invariant low <= x && x <= low + d;
   invariant high - d <= z && z <= high;
   invariant (forall i: int :: i < low ==> b[i] == a[i]);
   invariant (forall i: int ::
             0 <= i && i < x - low
             ==>
             seq(b, low, high)[i] == seq(a, low, high)[i + (high - low - d)]);
   invariant (forall i: int ::
             x - low <= i && i < high - low - d 
             ==>
             seq(b, low, high)[i] == seq(a, low, high)[i]);
   invariant (forall i: int ::
             high - low - d <= i && i < z - low   
             ==>
             seq(b, low, high)[i] == seq(a, low, high)[i - (high - low - d)]);
   invariant (forall i: int :: z <= i ==> b[i] == a[i]);
   {  // swap b[x] and b[z]
      tmp := b[z];
      b[z] := b[x];
      b[x] := tmp;
      x, z := x + 1, z + 1;
   }
}
